id = null;
slug = null;
title = null;
description = null;
passPercentage = null;
size = 'small';
moduleId = null;

function Project(project) {
    if (project.id) {
        this.id = parseInt(project.id);
    }
    if (project.title) {
        this.title = project.title;
    }
    if (project.slug) {
        this.slug = project.slug;
    } else {
        const newString = this.title.replace(/\W/g, '-');
        this.slug = newString.toLowerCase();
    }
    if (project.description) {
        this.description = project.description;
    }
    if (project.passPercentage) {
        this.passPercentage = parseInt(project.passPercentage);
    }
    if (project.size) {
        this.size = project.size;
    }
    if (project.moduleId) {
        this.moduleId = parseInt(project.moduleId);
    }
}

module.exports = Project;