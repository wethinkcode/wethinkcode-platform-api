id = 0;
title = '';
curriculum = [];
milestones = [];
slug = '';

function Module(module) {
    "use strict";
    if (module.id) {
        this.id = module.id;
    }
    if (module.title) {
        this.title = module.title;
    }
    if (module.slug) {
        this.slug = module.slug;
    } else {
        this.slug = this.generateSlug();
    }
    if (module.curriculum) {
        this.curriculum = module.curriculum;
    }
    if (module.milestones) {
        this.milestones = module.milestones
    }
}

Module.prototype.generateSlug = function generateSlug() {
    "use strict";
    const newString = this.title.replace(/\W/g, '-');
    return newString.toLowerCase();
};

module.exports = Module;