id = 0;
slug = '';
title = '';
description = '';
physicalFloor = '';
physicalUnit = '';
physicalBuilding = '';
physicalStreet = '';
physicalCity = '';
physicalProvince = '';
physicalCountry = '';
physicalPostalCode = '';
postalFloor = '';
postalUnit = '';
postalBuilding = '';
postalStreet = '';
postalCity = '';
postalProvince = '';
postalCountry = '';
postalPostalCode = '';
profileImage = '';
coverImage = '';

function Campus(campus) {
    if (campus) {
        if (campus.id) {
            this.id = campus.id;
        }
        if (campus.slug) {
            this.slug = campus.slug;
        }
        if (campus.title) {
            this.title = campus.title;
        }
        if (campus.description) {
            this.description = campus.description;
        }
        if (campus.physicalFloor) {
            this.physicalFloor = campus.physicalFloor;
        }
        if (campus.physicalUnit) {
            this.physicalUnit = campus.physicalUnit;
        }
        if (campus.physicalBuilding) {
            this.physicalBuilding = campus.physicalBuilding;
        }
        if (campus.physicalStreet) {
            this.physicalStreet = campus.physicalStreet;
        }
        if (campus.physicalCity) {
            this.physicalCity = campus.physicalCity;
        }
        if (campus.physicalProvince) {
            this.physicalProvince = campus.physicalProvince;
        }
        if (campus.physicalCountry) {
            this.physicalCountry = campus.physicalCountry
        }
        if (campus.physicalPostalCode) {
            this.physicalPostalCode = campus.physicalPostalCode
        }
        if (campus.postalFloor) {
            this.postalFloor = campus.postalFloor
        }
        if (campus.postalUnit) {
            this.postalUnit = campus.postalUnit;
        }
        if (campus.postalBuilding) {
            this.postalBuilding = campus.postalBuilding;
        }
        if (campus.postalStreet) {
            this.postalStreet = campus.postalStreet;
        }
        if (campus.postalCity) {
            this.postalCity = campus.postalCity;
        }
        if (campus.postalProvince) {
            this.postalProvince = campus.postalProvince;
        }
        if (campus.postalCountry) {
            this.postalCountry = campus.postalCountry
        }
        if (campus.postalPostalCode) {
            this.postalPostalCode = campus.postalPostalCode
        }
    }
}

Campus.prototype.generateSlug = function generateSlug() {
    const newString = this.title.replace(/\W/g, '-');
    return newString.toLowerCase();
};

module.exports = Campus;