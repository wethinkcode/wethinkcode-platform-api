const Campus = require('./campus');

id = 0;
slug = '';
title = '';
start_date = new Date();
campus = new Campus();
campuses = [];
student_count = 0;

function Cohort(cohort) {
    if (cohort.id) {
        this.id = cohort.id;
    }
    if (cohort.title) {
        this.title = cohort.title;
    }
    if (cohort.slug) {
        this.slug = cohort.slug;
    } else {
        this.slug = this.generateSlug();
    }
    if (cohort.startDate) {
        this.start_date = cohort.startDate;
    }
    if (cohort.campus) {
        this.campus = new Campus(cohort.campus);
    }
}

Cohort.prototype.generateSlug = function generateSlug() {
    if (this.title) {
        const newString = this.title.replace(/\W/g, '-');
        return newString.toLowerCase();
    }
};

module.exports = Cohort;