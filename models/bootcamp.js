id = 0;
slug = '';
title = '';
startDate = new Date();
endDate = new Date();
selectionDate = new Date();


function Bootcamp(bootcamp) {
    console.log(bootcamp);
    if (bootcamp.id) {
        this.id = bootcamp.id;
    }
    if (bootcamp.title) {
        this.title = bootcamp.title;
    }
    if (bootcamp.slug) {
        this.slug = bootcamp.slug;
    } else {
        this.slug = this.generateSlug();
    }
    if (bootcamp.startDate) {
        this.startDate = bootcamp.startDate;
    }
    if (bootcamp.endDate) {
        this.endDate = bootcamp.endDate;
    }
    if (bootcamp.selectionDate) {
        this.selectionDate = bootcamp.selectionDate;
    }
 }

Bootcamp.prototype.generateSlug = function generateSlug() {
    const newString = this.title.replace(/\W/g, '-');
    return newString.toLowerCase();
};

module.exports = Bootcamp;