id = 0;
title = '';
campuses = [];
slug = '';

function Curriculum(curriculum) {
    "use strict";
    if (curriculum.id) {
        this.id = curriculum.id;
    }
    if (curriculum.title) {
        this.title = curriculum.title;
    }
    if (curriculum.slug) {
        this.slug = curriculum.slug;
    }
    if (curriculum.campuses) {
        this.campuses = curriculum.campuses;
    }
}

Curriculum.prototype.generateSlug = function generateSlug() {
    "use strict";
    const newString = this.title.replace(/\W/g, '-');
    return newString.toLowerCase();
};

module.exports = Curriculum;