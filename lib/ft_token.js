const jwt = require('jsonwebtoken');

function verifyToken(req){
	"use strict";
	let bearerToken;
	let bearerHeader = req.headers["authorization"];
	if (typeof bearerHeader !== 'undefined') {
		let bearer = bearerHeader.split(" ");
		bearerToken = bearer[1];
		try {
			return jwt.verify(bearerToken, process.env.JWT_SECRET);
		} catch(err) {
			return null
		}
	}
	return null;
}

module.exports = {
	verifyToken
};