const express = require('express'),
    path = require('path'),
    favicon = require('serve-favicon'),
    logger = require('morgan'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'),
    sassMiddleware = require('node-sass-middleware'),
    io = require('socket.io-client')('https://wethinkcode-autobot-production.herokuapp.com');

io.on('connection-established', data => {
    console.log('Connected to Socket server');
});

io.on('process-started', processData => {
    console.log(processData);
});

io.on('process-completed', processData => {
    "use strict";
    console.log(processData);
});

io.on('process-failed', processData => {
    "use strict";
    console.log(processData);
});

io.on('process-status', processData => {
    "use strict";
    console.log(processData);
});


const index = require('./routes/index'),
    users = require('./routes/users'),
    students = require('./routes/students'),
	projects = require('./routes/projects'),
	modules = require('./routes/modules'),
	milestones = require('./routes/milestones'),
    tools = require('./routes/tools'),
    campuses = require('./routes/campuses'),
    curriculum = require('./routes/curriculum'),
    reports = require('./routes/reports'),
    bootcampers = require('./routes/bootcampers'),
    bootcamps = require('./routes/bootcamps'),
    documentation = require('./routes/documentation'),
    staff = require('./routes/staff'),
    cohorts = require('./routes/cohorts');

const app = express();

app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
	next();
});

require('dotenv').config();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.locals.io = io;

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(sassMiddleware({
  src: path.join(__dirname, 'public'),
  dest: path.join(__dirname, 'public'),
  indentedSyntax: true, // true = .sass and false = .scss
  sourceMap: true
}));
app.use(express.static(path.join(__dirname, 'public')));

app.use(function(req, res, next) {
    if (req.originalUrl !== req.baseUrl + req.url) {
        res.redirect(301, req.baseUrl + req.url);
    }
    else
        next();
});

app.use('/', index);
app.use('/users', users);
app.use('/students', students);
app.use('/projects', projects);
app.use('/campuses', campuses);
app.use('/modules', modules);
app.use('/milestones', milestones);
app.use('/reports', reports);
app.use('/tools', tools);
app.use('/curriculum', curriculum);
app.use('/bootcampers', bootcampers);
app.use('/bootcamps', bootcamps);
app.use('/documentation', documentation);
app.use('/staff', staff);
app.use('/cohorts', cohorts);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


module.exports = app;
