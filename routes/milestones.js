const express = require('express');
const router = express.Router();

const db = require('../db').db;
const ft_string = require('../lib/ft_string');
const ft_token = require('../lib/ft_token');

// Fetch All milestones
router.get('/', (req, res, next) => {
    const token = ft_token.verifyToken(req);
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'We could not verify your token.',
            origin: 'Campus endpoint - New campus',
            priority: 9
        })
    } else {
        "use strict";
        let orderBy = 'title';
        let page = 1;
        let limit = 20;
        let yieldValues = [];
        let returnValue = [];
        if (req.query.page) {
            page = parseInt(req.query.page);
        }
        if(req.query.results_per_page) {
            limit = parseInt(req.query.results_per_page);
        }
        if (req.query.order_by) {
            switch(req.query.order_by) {
                case 'slug':
                    orderBy = req.query.order_by;
                    break;
                default: break;
            }
        }
        db.task('fetch-milestones', function *(t) {
            return t.any('SELECT * FROM milestones ORDER BY ' + orderBy + ' LIMIT ' + limit + 'OFFSET ' +
            limit * (page - 1));
        }).then(result => {
            return res.json({
                code: 'S0002',
                status: 'success',
                message: 'Results Retrieved Successfully',
                data: result,
                origin: 'Milestones endpoint - Fetch milestone users',
                priority: 10
            })
        }).catch(err => {
            console.log(err);
            return res.json({
                code: 'S0003',
                status: 'failure',
                message: 'An error has occurred.',
                data: err,
                origin: 'Milestones endpoint - Fetch milestone users',
                priority: 8
            })
        });
    }
});

router.get('/:id', (req, res, next) => {
	"use strict";
    res.send('Fetching single milestone');
});

router.get('/:id/users', (req, res, next) => {
	"use strict";
    let orderBy = 'first_name';
    let page = 1;
    let limit = 20;
    let yieldValues = [];
    let returnValue = [];
    let query = 'SELECT users_students.id AS users_students_id, users_students.level_42, ' +
		'users_students.level_validation, users_students.year, users_students.status, users.id, users.first_name, ' +
		'users.last_name, users.display_name, users.profile_image, users.username, milestones.id, milestones.title, ' +
		'milestones.slug, users_milestones.is_validated FROM users_milestones INNER JOIN users ON ' +
		'users_milestones.user_id = users.id INNER JOIN milestones ON users_milestones.milestone_id = milestones.id ' +
		'INNER JOIN users_students ON users_milestones.user_id = users_students.user_id WHERE';
    let id = req.params.id;
    if (req.query.page) {
        page = parseInt(req.query.page);
    }
    if(req.query.results_per_page) {
        limit = parseInt(req.query.results_per_page);
    }
    if (req.query.order_by) {
        switch(req.query.order_by) {
            case 'last_name':
                orderBy = req.query.order_by;
                break;
            default: break;
        }
    }
    db.task('fetch-milestone-users', function *(t) {
    	if (parseInt(id))
            yieldValues = yield t.any(query + ' milestones.id = $1 ORDER BY ' + orderBy + ' LIMIT ' + limit + 'OFFSET '
				+ limit * (page - 1), [parseInt(id)]);
        else
			yieldValues = yield t.any(query + ' milestones.slug = $1 ORDER BY ' + orderBy + ' LIMIT ' + limit +
				'OFFSET ' + limit * (page - 1), [req.params.id]);
        for (let yieldValue of yieldValues) {
            returnValue.push({
                username: yieldValue.username,
                first_name: yieldValue.first_name,
                last_name: yieldValue.last_name,
                display_name: yieldValue.display_name,
                profile_image: yieldValue.profile_image,
				student: {
					id: yieldValue.users_students_id,
					level_42: yieldValue.level_42,
                    level_wtc: yieldValue.level_validation,
                    year: yieldValue.year,
                    status: yieldValue.status
				},
                milestone: {
                    id: yieldValue.id,
                    title: yieldValue.title,
                    slug: yieldValue.slug,
                    is_validated: yieldValue.is_validated
                }
            });
        }
        return returnValue;
    }).then(result => {
        return res.json({
            code: 'S0001',
            status: 'success',
            message: 'Results Retrieved Successfully',
            data: result,
            origin: 'Milestones endpoint - Fetch milestone users',
            priority: 10
        })
    }).catch(error => {
    	console.log(error);
        return res.json({
            code: 'S0002',
            status: 'failure',
            message: 'An error has occurred.',
            data: error,
            origin: 'Milestones endpoint - Fetch milestone users',
            priority: 8
        })
    });

});

module.exports = router;