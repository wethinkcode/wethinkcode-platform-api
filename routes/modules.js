const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');

// Fetch All modules
const db = require('../db').db;
const ft_string = require('../lib/ft_string');
const ft_token = require('../lib/ft_token');
const Module = require('../models/module');

router.get('/', (req, res, next) => {
    "use strict";
    const token = ft_token.verifyToken(req);
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'We could not verify your token.',
            origin: 'Student Endpoint - Fetch modules',
            priority: 9
        })
    } else {
        db.task('Get student modules', function *(t) {
            return yield t.any('SELECT * FROM modules');
        }).then(result => {
            return res.json({
                code: 'S0002',
                status: 'success',
                message: 'Results found.',
                data: result,
                origin: 'Student Endpoint - Fetch modules',
                priority: 10
            })
        }).catch(err => {
            console.log(err);
            return res.json({
                code: 'S0003',
                status: 'failure',
                message: 'An error has occurred.',
                data: err,
                origin: 'Student Endpoint - Fetch modules',
                priority: 8
            })
        })
    }
});

router.post('/', (req, res, next) => {
    const token = ft_token.verifyToken(req);
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'We could not verify your token.',
            origin: 'Curriculum endpoint - New curriculum',
            priority: 9
        })
    } else if (token.role !== 'administrator') {
        return res.json({
            code: 'S0002',
            status: 'failure',
            message: 'Access Denied.',
            origin: 'Curriculum endpoint - New curriculum',
            priority: 9
        })
    } else {
        if (!req.body.title) {
            return res.json({
                code: 'S0003',
                status: 'failure',
                message: 'A campus title is required. Please try again.',
                origin: 'Curriculum endpoint - New curriculum',
                priority: 9
            })
        } else {
            let module = new Module(req.body);
            let queryArray = [module.slug, module.title];
            db.task('Add Module', t => {
                return t.one('INSERT INTO modules(slug, title) VALUES($1, $2) RETURNING id',
                    queryArray).then(result => {
                    if (result.id && module.curriculum && module.curriculum.length) {
                        for (const curriculumSingle of module.curriculum) {
                            t.none('INSERT INTO modules_curriculum(module_id, curriculum_id) VALUES($1, $2)', [curriculumSingle.id, result.id])
                                .catch(resultModulesCurriculum => {
                                    "use strict";
                                    throw(resultModulesCurriculum);
                                });
                        }
                    }
                    if (result.id && module.milestones && module.milestones.length) {
                        for (const milestone of module.milestones) {
                            t.none('INSERT INTO modules_milestones(module_id, milestone_id) VALUES($1, $2)', [milestone.id, result.id])
                                .catch(resultMilestoneCurriculum => {
                                    "use strict";
                                    throw(resultMilestoneCurriculum);
                                });
                        }
                    }
                })
            }).then(result => {
                return res.json({
                    code: 'S0003',
                    status: 'success',
                    message: 'Record added successfully',
                    origin: 'Module endpoint - New module',
                    priority: 10
                })
            }).catch( error => {
                console.log(error);
                if (error) {
                    return res.json({
                        code: 'S0004',
                        status: 'failure',
                        message: 'An error has occurred.',
                        data: error,
                        origin: 'Module endpoint - New module',
                        priority: 8
                    })
                }
            });
        }
    }
});

module.exports = router;