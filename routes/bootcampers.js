const express = require('express');
const router = express.Router();
const apicache = require('apicache');

const db = require('../db').db;
const ft_string = require('../lib/ft_string');
const ft_token = require('../lib/ft_token');

// get bootcamper
router.get('/:id', (req, res, next) => {
    const origin = 'Bootcamper endpoint - Get Bootcamper';
    let token = ft_token.verifyToken(req);
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'Could not verify your webToken. Please contact support. (Code: S0001)',
            origin: 'Bootcamper endpoint - Get Bootcamper',
            priority: 8
        })
    } else if (!req.params.id){
        return res.json({
            code: 'S0002',
            status: 'failure',
            message: 'An "id" query parameter is required. Please try again.',
            origin: origin,
            priority: 9
        })
    } else {
        let query = 'users.id = $1';
        if (!parseInt(req.params.id)) {
            query = 'users.username = $1';
        }
        db.one('SELECT users.id, users.first_name, users.last_name, users.display_name, users.email, ' +
            'users.role, users.profile_image, users.cover_image, users.username, users_bootcampers.bootcamp_month,' +
            'users_bootcampers.bootcamp_year, users_bootcampers.level, users_bootcampers.final_decision FROM users INNER JOIN users_bootcampers ON ' +
            'users.id = users_bootcampers.user_id WHERE ' + query, [req.params.id]).then(user => {
            return res.json({
                code: 'S0003',
                status: 'success',
                message: 'Results retrieved successfully',
                data: user,
                origin: origin,
                priority: 10
            })
        }).catch((err) => {
            console.log(err);
            return res.json({
                code: 'S0004',
                status: 'failure',
                message: 'An error has occurred.',
                data: err,
                origin: origin,
                priority: 8
            });
        })
    }
});

// get bootcamper projects
router.get('/:id/projects', (req, res, next) => {
    const origin = 'Bootcamper endpoint - Get bootcamper projects';
    let token = ft_token.verifyToken(req);
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'Could not verify your webToken. Please contact support. (Code: S0001)',
            origin: 'Bootcamper endpoint - Get Bootcamper',
            priority: 8
        })
    } else if (!req.params.id){
        return res.json({
            code: 'S0002',
            status: 'failure',
            message: 'An "id" query parameter is required. Please try again.',
            origin: origin,
            priority: 9
        })
    } else {
        let orderBy = 'users_projects.started_at';
        let page = 1;
        let limit = 20;
        if (req.query.page) {
            page = parseInt(req.query.page);
        }
        if (req.query.results_per_page) {
            limit = parseInt(req.query.results_per_page);
        }
        if (req.query.order_by) {
            switch (req.query.order_by) {
                case 'username':
                    orderBy = 'users.username';
                    break;
                default:
                    break;
            }
        }
        let query = 'users.id = $1';
        if (!parseInt(req.params.id)) {
            query = 'users.username = $1';
        }
        db.any('SELECT projects.id, projects.title, projects.pass_percentage, projects.size, projects.description, ' +
            'projects.description, projects.slug, projects.module_id, users_projects.started_at, users_projects.ended_at,' +
            'users_projects.final_mark FROM users_projects INNER JOIN projects ON users_projects.project_id = ' +
            'projects.id INNER JOIN users ON users_projects.user_id = users.id WHERE ' + query +
            'ORDER BY ' + orderBy + ' LIMIT ' + limit + 'OFFSET ' + limit * (page - 1), [req.params.id]).then(projects => {
            return res.json({
                code: 'S0003',
                status: 'success',
                message: 'Results retrieved successfully',
                data: projects,
                origin: origin,
                priority: 10
            })
        }).catch((err) => {
            console.log(err);
            return res.json({
                code: 'S0004',
                status: 'failure',
                message: 'An error has occurred.',
                data: err,
                origin: origin,
                priority: 8
            });
        })
    }
});

router.post('/:id/votes', (req, res, next) => {
    "use strict";
    const origin = 'Bootcamps Endpoint - New bootcamper vote';
    const token = ft_token.verifyToken(req);
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'We could not verify your token.',
            origin: origin,
            priority: 9
        })
    } else {
        if (!req.body.role) {
            return res.json({
                code: 'S0002',
                status: 'failure',
                message: 'A vote role is required in the body of your request. please try again.',
                origin: origin,
                priority: 9
            })
        } else if (!req.body.type) {
            return res.json({
                code: 'S0002',
                status: 'failure',
                message: 'A vote type id is required in the body of your request. please try again.',
                origin: origin,
                priority: 9
            })
        } else if (!req.body.bootcamp_id) {
            return res.json({
                code: 'S0002',
                status: 'failure',
                message: 'A bootcamp id is required in the body of your request. Please try again.',
                origin: origin,
                priority: 9
            })
        } else {
            let query = '$2';
            if (isNaN(req.params.id)) {
                query = '(SELECT id from users WHERE username = $2)';
            }
            return db.oneOrNone('INSERT INTO users_votes(voter_id, votee_id, type, date, role, bootcamp_id) ' +
                'VALUES($1, '+ query + ', $3, $4, $5, $6) ON CONFLICT (voter_id, votee_id, type, role, bootcamp_id)' +
                'DO NOTHING RETURNING id', [])
                .then(user => {
                    if (user) {
                        return res.json({
                            code: 'S0002',
                            status: 'success',
                            message: 'Vote placed successfully.',
                            data: user,
                            origin: origin,
                            priority: 9
                        })
                    } else {
                        return res.json({
                            code: 'S0002',
                            status: 'failure',
                            message: 'That vote has already been placed.',
                            origin: origin,
                            priority: 9
                        })

                    }
                }).catch(error => {
                    return res.json({
                        code: 'S0004',
                        status: 'failure',
                        message: 'An error has occurred.',
                        data: err,
                        origin: origin,
                        priority: 8
                    });
            });
        }
    }
});

router.patch('/:id/decisions', (req, res, next) => {
    "use strict";
    const origin = 'Bootcamps Endpoint - New bootcamper vote';
    const token = ft_token.verifyToken(req);
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'We could not verify your token.',
            origin: origin,
            priority: 9
        })
    } else if (token.role !== 'administrator' && token.role !== 'staff') {
        return res.json({
            code: 'S0002',
            status: 'failure',
            message: 'Access denied.',
            origin: origin,
            priority: 9
        })
    } else if (!token.id) {
        return res.json({
            code: 'S0003',
            status: 'failure',
            message: 'a token id is required.',
            origin: origin,
            priority: 9
        })
    } else if (!req.body.bootcampId) {
        return res.json({
            code: 'S0004',
            status: 'failure',
            message: 'A valid bootcamp id in the body of your request. Please try again.',
            origin: origin,
            priority: 9
        });
    } else if (!req.body.type || (req.body.type !== 'accepted' && req.body.type !== 'declined' &&
            req.body.type !== 'retry' && req.body.type !== 'retract')) {
        return res.json({
            code: 'S0005',
            status: 'failure',
            message: 'A valid decision type is required in the body of your request. Please try again.',
            origin: origin,
            priority: 9
        });
    } else {
        let query = 'user_id = $2';
        if (isNaN(req.params.id)) {
            query = 'user_id = (SELECT id FROM users WHERE username = $2)';
        }
        let type = req.body.type;
        let decision = 'final_decision = $1, deciding_user_id = $3';
        if (req.body.type === 'retract') {
            decision = 'final_decision = null, deciding_user_id = null';
        }
        db.task(t => {
            return t.oneOrNone('SELECT id FROM users_bootcamps WHERE ' + query + ' AND bootcamp_id = $3 AND final_decision IS NOT null LIMIT 1', [type, req.params.id, req.body.bootcampId]).then(result => {
                if (result && req.body.type !== 'retract') {
                    return res.json({
                        code: 'S0006',
                        status: 'failure',
                        message: 'A final decision has already been made for this user.',
                        origin: origin,
                        priority: 9
                    });
                } else {
                    return t.oneOrNone('UPDATE users_bootcamps SET ' + decision + ' WHERE ' + query + ' AND bootcamp_id = $4 RETURNING id',
                        [type, req.params.id, token.id, req.body.bootcampId]).then(updatedUser => {
                        if(!updatedUser) {
                            throw res.json({
                                code: 'S0007',
                                status: 'failure',
                                message: 'That user could not be updated',
                                origin: origin,
                                priority: 9
                            });
                        }
                    }).then(updateResult => {
                        return res.json({
                            code: 'S0008',
                            status: 'success',
                            message: 'User decision updated successfully.',
                            origin: origin,
                            priority: 9
                        });
                    }).catch(error => {
                        return res.json({
                            code: 'S0009',
                            status: 'failure',
                            message: 'An error has occurred.',
                            data: error,
                            origin: origin,
                            priority: 8
                        });
                    })
                }
            }).catch(error => {
                return res.json({
                    code: 'S0010',
                    status: 'failure',
                    message: 'An error has occurred.',
                    data: error,
                    origin: origin,
                    priority: 8
                });
            })
        });
    }
});


module.exports = router;