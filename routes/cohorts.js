const express = require('express');
const router = express.Router();
const Cohort = require('../models/cohort');

const db = require('../db').db;
const ft_token = require('../lib/ft_token');
const ft_string = require('../lib/ft_string');

router.get('/:id', (req, res, next) => {
    "use strict";
    const origin = 'Cohorts endpoint - Fetch cohort by id';
    const token = ft_token.verifyToken(req);
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'We could not verify your token.',
            origin: origin,
            priority: 9
        })
    } else if (token.role !== 'administrator' && token.role !== 'staff') {
        return res.json({
            code: 'S0002',
            status: 'failure',
            message: 'Access denied.',
            origin: origin,
            priority: 9
        })
    } else if (!req.params.id) {
        return res.json({
            code: 'S0002',
            status: 'failure',
            message: 'A cohort id is required. Please try again.',
            origin: origin,
            priority: 9
        })
    } else {
        let query = 'id = $1';
        if (isNaN(req.params.id)) {
            query = 'slug = $1';
        }
        let cohort = {};
        db.task( t => {
            return t.oneOrNone('SELECT id, title, slug, start_date, campus_id FROM cohorts WHERE ' + query + ' LIMIT 1', [req.params.id]);
        }).then(cohortResult => {
            if (!cohortResult) {
                throw res.json({
                    code: 'S0001',
                    status: 'failure',
                    message: 'That cohort does not exist. Please try again.',
                    origin: origin,
                    priority: 9
                })
            } else {
                cohort = cohortResult;
                return db.tx(t => {
                    query = 'users_cohorts.cohort_id = $1';
                    if (isNaN(req.params.id)) {
                        query = 'users_cohorts.cohort_id = (SELECT id FROM cohorts WHERE slug = $1)';
                    }
                    return t.any('SELECT users.id, users.username, users.first_name, users.last_name, users_students.state FROM users INNER JOIN ' +
                        'users_cohorts on users.id = users_cohorts.user_id INNER JOIN users_students ON users.id = users_students.user_id WHERE ' + query, [req.params.id])
                        .then(users => {
                            cohort.users = users;
                            const queries = [];
                            for (const user of cohort.users) {
                                queries.push(t.any('SELECT projects.id, projects.title, projects.pass_percentage, projects.size, projects.description, ' +
                                    'projects.description, projects.slug, projects.module_id, users_projects.started_at, users_projects.ended_at,' +
                                    'users_projects.final_mark FROM users_projects INNER JOIN projects ON users_projects.project_id = ' +
                                    'projects.id INNER JOIN users ON users_projects.user_id = users.id WHERE user_id = $1', user.id).then(userProjectsResults => {
                                    user.projects = userProjectsResults;
                                }));
                                queries.push(t.any('SELECT users_exams.exam_id, users_exams.grade, ' +
                                    'projects.pass_percentage FROM users_exams INNER JOIN projects on ' +
                                    'users_exams.exam_id = projects.id WHERE users_exams.user_id = $1 AND ' +
                                    'users_exams.exam_id = 510', [user.id]).then(userExamsResults => {
                                    user.exams = userExamsResults;
                                }));
                                queries.push(t.any('SELECT users_modules.module_id AS id, modules.title, ' +
                                    'modules.slug FROM users_modules INNER JOIN modules on users_modules.module_id = ' +
                                    'modules.id WHERE users_modules.user_id = $1', [user.id]).then(userModulesResults => {
                                    user.modules = userModulesResults;
                                }));
                                queries.push(t.one('SELECT COUNT(*) FROM (SELECT DISTINCT ON (date(access_date)) access_date ' +
                                    'FROM users_attendance WHERE date_trunc(\'month\', access_date) = date_trunc(\'month\', now() - interval \'1\' month) AND ' +
                                    'user_id = $1) AS access', [user.id]).then(userAttendanceResults => {
                                    user.attendance = userAttendanceResults.count;
                                }));
                            }
                            return t.batch(queries);
                        })
                        .catch(usersError => {
                            console.log(usersError);
                            throw res.json({
                                code: 'S0001',
                                status: 'failure',
                                message: 'We could not retrieve cohort users. Please try again.',
                                origin: origin,
                                priority: 8
                            })
                        })
                }).then(studentReportsResult => {
                    return res.json({
                        code: 'S0003',
                        status: 'success',
                        message: 'Results Retrieved Successfully',
                        data: cohort,
                        origin: origin,
                        priority: 10
                    })
                })

            }
        });
    }
});

router.get('/', (req, res, next) => {
    const origin = 'Cohorts endpoint - Fetch all cohorts';
    const token = ft_token.verifyToken(req);
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'We could not verify your token.',
            origin: origin,
            priority: 9
        })
    } else if (token.role !== 'administrator' && token.role !== 'staff') {
        return res.json({
            code: 'S0002',
            status: 'failure',
            message: 'Access denied.',
            origin: origin,
            priority: 9
        })
    } else {
        let orderBy = 'title';
        let page = 1;
        let limit = 20;
        if (req.query.page) {
            page = parseInt(req.query.page);
        }
        if (req.query.results_per_page) {
            limit = parseInt(req.query.results_per_page);
        }
        if (req.query.order_by) {
            switch (req.query.order_by) {
                case 'slug':
                    orderBy = req.query.order_by;
                    break;
                default:
                    break;
            }
        }
        let cohorts = [];
        db.task(t =>  {
            return t.any('SELECT * FROM cohorts ORDER BY ' + orderBy + ' LIMIT ' + limit + 'OFFSET ' + limit * (page - 1))
        }).then(resultCohorts => {
            cohorts = resultCohorts;
            return db.tx(t => {
                const queries = [];
                for (const cohort of cohorts) {
                    queries.push(t.one('SELECT * FROM campuses WHERE id = $1 LIMIT 1', cohort.campus_id).then(campus => {
                        cohort.campus = campus
                    }));
                    queries.push(t.one('SELECT COUNT(id) FROM users_cohorts WHERE cohort_id = $1', cohort.id).then(users => {
                        "use strict";
                        if (users) {
                            cohort.student_count = parseInt(users.count);
                        }
                    }))
                }
                return t.batch(queries);
            })
        }).then(taskResult => {
            return res.json({
                code: 'S0003',
                status: 'success',
                message: 'Results Retrieved Successfully',
                data: cohorts,
                origin: origin,
                priority: 10
            })
        }).catch(error => {
            console.log(error);
            return res.json({
                code: 'S0004',
                status: 'failure',
                message: 'An error has occurred.',
                data: error,
                origin: origin,
                priority: 8
            })
        });
    }
});

router.post('/', (req, res, next) => {
    const origin = 'Cohorts Endpoint - New cohort';
    const token = ft_token.verifyToken(req);
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'We could not verify your token.',
            origin: origin,
            priority: 9
        })
    } else if (token.role !== 'administrator') {
        return res.json({
            code: 'S0002',
            status: 'failure',
            message: 'Access Denied.',
            origin: origin,
            priority: 9
        })
    } else {
        if (!req.body.title) {
            return res.json({
                code: 'S0003',
                status: 'failure',
                message: 'A cohort title is required. Please try again.',
                origin: origin,
                priority: 9
            })
        } else if (!req.body.startDate) {
            return res.json({
                code: 'S0004',
                status: 'failure',
                message: 'A cohort start date is required. Please try again.',
                origin: origin,
                priority: 9
            })
        } else if (!req.body.campus) {
            return res.json({
                code: 'S0005',
                status: 'failure',
                message: 'A cohort campus is required. Please try again.',
                origin: origin,
                priority: 9
            })
        } else {
            let cohort = new Cohort(req.body);
            let queryArray = [cohort.slug, cohort.title, cohort.start_date, cohort.campus.id];
            let id = 0;
            db.task(t => {
                "use strict";
                return t.oneOrNone('SELECT * FROM cohorts WHERE title = $1 OR slug = $2 LIMIT 1', [cohort.title, cohort.slug])
                    .then(cohortResult => {
                        if (cohortResult) {
                            throw res.json({
                                code: 'S0006',
                                status: 'failure',
                                message: 'That cohort already exists. Please try again.',
                                origin: origin,
                                priority: 8
                            })
                        }
                    }).catch(errorSelectingCohort => {
                        throw res.json({
                            code: 'S0007',
                            status: 'failure',
                            message: 'We could not verify the provided cohort. Please try again.',
                            origin: origin,
                            priority: 8
                        })
                    }).then(findCohortResult => {
                        return t.oneOrNone('SELECT id FROM campuses WHERE id = $1', cohort.campus.id)
                            .then(selectCampusResult => {
                                console.log(selectCampusResult);
                                if (!selectCampusResult) {
                                    throw res.json({
                                        code: 'S0008',
                                        status: 'failure',
                                        message: 'That campus does not exist. Please try again.',
                                        origin: origin,
                                        priority: 8
                                    });
                                }
                            })
                            .catch(errorSelectingCampus =>{
                                throw res.json({
                                    code: 'S0009',
                                    status: 'failure',
                                    message: 'We could not verify the provided campus. Please try again.',
                                    origin: origin,
                                    priority: 8
                                })
                            });
                    }).catch(selectCampusError => {
                        throw selectCampusError;
                    }).then(selectResult => {
                        return t.one('INSERT INTO cohorts(slug, title, start_date, campus_id) ' +
                            'VALUES($1, $2, $3, $4) RETURNING id', queryArray)
                            .then(insertCohortResult => {
                                if (insertCohortResult) {
                                    id = insertCohortResult.id;
                                }
                            }).catch(insertError => {
                                throw res.json({
                                    code: 'S0009',
                                    status: 'failure',
                                    message: 'We could create the requested cohort. Please try again.',
                                    origin: origin,
                                    priority: 8
                                })
                            })
                    })
            }).then(result => {
                "use strict";
                return res.json({
                    code: 'S0011',
                    status: 'success',
                    message: 'Cohort added Successfully',
                    data: {id: id},
                    origin: origin,
                    priority: 10
                })
            })
        }
    }
});

module.exports = router;