const express = require('express');
const router = express.Router();

const db = require('../db').db;
const ft_token = require('../lib/ft_token');
const ft_string = require('../lib/ft_string');

router.get('/', (req, res, next) => {
    "use strict";
    const origin = 'Staff Endpoint - Get staff members';
    const token = ft_token.verifyToken(req);
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'We could not verify your token.',
            origin: origin,
            priority: 9
        })
    } else if (token.role !== 'administrator' && token.role !== 'staff') {
        return res.json({
            code: 'S0002',
            status: 'failure',
            message: 'Access Denied.',
            origin: 'Projects endpoint - New project',
            priority: 9
        })
    } else {
        let orderBy = 'username';
        let page = 1;
        let limit = 30;
        if (req.query.page) {
            page = parseInt(req.query.page);
        }
        if (req.query.results_per_page) {
            limit = parseInt(req.query.results_per_page);
        }
        if (req.query.order_by) {
            switch (req.query.order_by) {
                case 'first_name':
                    orderBy = 'first_name';
                    break;
                default:
                    break;
            }
        }
        db.task(t =>  {
            return t.any('SELECT id, username, first_name, last_name, email, role, profile_image FROM users WHERE role = \'staff\' OR role = \'administrator\' ORDER BY ' + orderBy + ' LIMIT ' + limit + 'OFFSET ' + limit * (page - 1));
        }).then(result => {
            return res.json({
                code: 'S0003',
                status: 'success',
                message: 'Results retrieved successfully',
                data: result,
                origin: origin,
                priority: 10
            })
        }).catch(error => {
            return res.json({
                code: 'S0004',
                status: 'failure',
                message: 'An error has occurred.',
                data: error,
                origin: origin,
                priority: 8
            })
        });
    }
});

module.exports = router;