const express = require('express');
const router = express.Router();
const apicache = require('apicache');

const db = require('../db').db;
const ft_string = require('../lib/ft_string');
const ft_token = require('../lib/ft_token');
let cache = apicache.middleware;

router.get('/', cache('5 minutes'), (req, res, next) => {
    let orderBy = 'first_name';
    let page = 1;
    let limit = 30;
    if (req.query.page) {
        page = parseInt(req.query.page);
    }
    if(req.query.results_per_page) {
        limit = parseInt(req.query.results_per_page);
    }
    if (req.query.order_by) {
        switch(req.query.order_by) {
            case 'last_name':
                orderBy = req.query.order_by;
                break;
            default: break;
        }
    }
    db.task('my-task', function * (t) {
	    return yield t.any('SELECT id, first_name, last_name, display_name, email, role, profile_image, ' +
            'username FROM users WHERE role=\'student\' ORDER BY ' + orderBy + ' LIMIT ' + limit + 'OFFSET ' +
            limit * (page - 1));
    }).then(result => {
        return res.json({
            code: 'S0001',
            status: 'success',
            message: 'Results Retrieved Successfully',
            data: result,
            origin: 'Student endpoint - Fetch students',
            priority: 8
        })
    }).catch(error => {
        return res.json({
            code: 'S0002',
            status: 'failure',
            message: 'An error has occurred.',
            data: error,
	        origin: 'Student endpoint - Fetch students',
            priority: 8
        })
    });
});


// fetch users milestones
router.get('/compliance', cache('5 minutes'), (req, res, next) => {
    "use strict";
    let orderBy = 'first_name';
    let page = 1;
    let limit = 10;
    let returnValue = [];
    if (req.query.page) {
        page = parseInt(req.query.page);
    }
    if(req.query.results_per_page) {
        limit = parseInt(req.query.results_per_page);
    }
    if (req.query.order_by) {
        switch(req.query.order_by) {
            case 'last_name':
                orderBy = req.query.order_by;
                break;
            default: break;
        }
    }
    db.task('fetch-users-milestones', function *(t) {
        let results = yield t.any('SELECT users.id, users.username, users.first_name, users.last_name, users.profile_image, users_students.level, users_students.year, users_students.state FROM users_students INNER JOIN users on users_students.user_id = users.id WHERE role = \'student\' ORDER BY ' + orderBy + ' LIMIT ' +
            limit + ' OFFSET ' + limit * (page - 1));
        for (let item of results) {
            item.milestones = yield t.any('SELECT users_milestones.id, milestones.title, users_milestones.is_validated FROM users_milestones INNER JOIN milestones ON users_milestones.milestone_id = milestones.id WHERE user_id = $1', [item.id])
            item.attendance = yield t.any('SELECT DISTINCT ON (date(access_date)) access_date FROM users_attendance WHERE access_date > NOW() - INTERVAL \'90 days\' AND user_id = $1 ORDER BY date(access_date) DESC', [item.id])
        }
        return results;
    }).then(result => {
        return res.json({
            code: 'S0001',
            status: 'success',
            message: 'Results Retrieved Successfully',
            data: result,
            origin: 'Students endpoint - Fetch milestone users',
            priority: 8
        })
    }).catch(error => {
        console.log(error);
        return res.json({
            code: 'S0002',
            status: 'failure',
            message: 'An error has occurred.',
            data: error,
            origin: 'Milestones endpoint - Fetch milestone users',
            priority: 8
        })
    });
});

// Get student attendance
router.get('/:id/attendance', (req, res, next) => {
    "use strict";
    const token = ft_token.verifyToken(req);
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'We could not verify your token.',
            origin: 'User Endpoint - New user quicklink',
            priority: 9
        })
    } else {
        if (req.params.id !== token.id && token.role !== 'administrator') {
            return res.json({
                code: 'S0002',
                status: 'failure',
                message: 'Access Denied.',
                origin: 'User Endpoint - Fetch user quicklinks',
                priority: 9
            })
        } else {
            let query = 'user_id = $1';
            let identifier = parseInt(req.params.id);
            if (!parseInt(req.params.id)) {
                query = 'username = $1';
                identifier = req.params.id;
            }  else {
            }
            db.task('Get student attendance', function *(t) {
                return yield t.any('SELECT DISTINCT ON (date(users_attendance.access_date)) users_attendance.access_date, ' +
                    'users_attendance.id FROM users_attendance INNER JOIN users ON ' +
                    'users_attendance.user_id = users.id WHERE ' + query + ' ORDER BY date(users_attendance.access_date) DESC LIMIT 120', [identifier])
            }).then(result => {
                return res.json({
                    code: 'S0003',
                    status: 'success',
                    message: 'Results found.',
                    data: result,
                    origin: 'User Endpoint - Fetch user Quick links',
                    priority: 10
                })
            }).catch(err => {
                console.log(err);
                return res.json({
                    code: 'S0004',
                    status: 'failure',
                    message: 'An error has occurred.',
                    data: err,
                    origin: 'Student Endpoint - Fetch student attendance',
                    priority: 8
                })
            })
        }
    }
});

router.get('/:id/modules', (req, res, next) => {
    "use strict";
    const token = ft_token.verifyToken(req);
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'We could not verify your token.',
            origin: 'User Endpoint - Fetch student projects',
            priority: 9
        })
    } else {
        let query = 'users_modules.user_id = $1';
        let identifier = parseInt(req.params.id);
        if (!parseInt(req.params.id)) {
            query = 'users_modules.user_id = (SELECT id FROM users WHERE username = $1)';
            identifier = req.params.id;
        }  else {
        }
        db.task('Get student modules', function *(t) {
            return yield t.any('SELECT modules.id, modules.slug, modules.title, ' +
                'users_modules.is_validated FROM users_modules INNER JOIN modules ON ' +
                'users_modules.module_id = modules.id WHERE ' + query + 'ORDER BY modules.title DESC', [identifier])
        }).then(result => {
            return res.json({
                code: 'S0002',
                status: 'success',
                message: 'Results found.',
                data: result,
                origin: 'User Endpoint - Fetch student projects',
                priority: 10
            })
        }).catch(err => {
            console.log(err);
            return res.json({
                code: 'S0003',
                status: 'failure',
                message: 'An error has occurred.',
                data: err,
                origin: 'Student Endpoint - Fetch student projects',
                priority: 8
            })
        })
    }
});

// Get student projects
router.get('/:id/projects', (req, res, next) => {
    const token = ft_token.verifyToken(req);
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'We could not verify your token.',
            origin: 'User Endpoint - Fetch student projects',
            priority: 9
        })
    } else {
        let query = 'users_projects.user_id = $1';
        let query2 = 'projects.module_id = $2';
        let module = parseInt(req.query.module);
        let identifier = parseInt(req.params.id);
        if (!parseInt(req.params.id)) {
            query = 'users_projects.user_id = (SELECT id FROM users WHERE username = $1 LIMIT 1)';
            identifier = req.params.id;
        }
        if (!parseInt(module)) {
            query2 = 'projects.module_id = (SELECT id FROM modules WHERE modules.slug = $2 LIMIT 1)';
            module = req.query.module;
        }
        db.task('Get student projects', function *(t) {
            if (req.query.module) {
                return yield t.any('SELECT projects.id, projects.module_id, projects.size, projects.title, ' +
                    'projects.slug, users_projects.final_mark, projects.pass_percentage FROM users_projects INNER JOIN projects ON ' +
                    'users_projects.project_id = projects.id WHERE ' + query + ' AND ' + query2, [identifier, module])
            } else {
                return yield t.any('SELECT projects.id, projects.module_id, projects.size, projects.title, ' +
                    'projects.slug, users_projects.final_mark, projects.pass_percentage FROM users_projects INNER JOIN projects ON ' +
                    'users_projects.project_id = projects.id WHERE ' + query, [identifier]);
            }
        }).then(result => {
            return res.json({
                code: 'S0003',
                status: 'success',
                message: 'Results found.',
                data: result,
                origin: 'User Endpoint - Fetch student projects',
                priority: 10
            })
        }).catch(err => {
            console.log(err);
            return res.json({
                code: 'S0004',
                status: 'failure',
                message: 'An error has occurred.',
                data: err,
                origin: 'Student Endpoint - Fetch student projects',
                priority: 8
            })
        })
    }
});

// get student
router.get('/:id', (req, res, next) => {
    "use strict";
    let token = ft_token.verifyToken(req);
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'Could not verify your webToken. Please contact support. (Code: S0001)',
            origin: 'User Endpoint - User update',
            priority: 8
        })
    } else if (!req.params.id){
        return res.json({
            code: 'S0002',
            status: 'failure',
            message: 'An "id" query parameter is required. Please try again.',
            origin: 'User Endpoint - User update',
            priority: 9
        })
    } else {
        let query = 'users.id = $1';
        if (!parseInt(req.params.id)) {
            query = 'users.username = $1';
        }
        let userResult = {};
        db.tx(t => {
            return t.oneOrNone('SELECT users.id, users.first_name, users.last_name, users.display_name, users.email, ' +
                'users.role, users.profile_image, users.cover_image, users.username, users_students.level, ' +
                'users_students.year, users_students.state FROM users INNER JOIN users_students ON ' +
                'users.id = users_students.user_id WHERE ' + query + ' LIMIT 1' , [req.params.id]).then(user => {
                    const queries = [];
                    userResult = user;
                    queries.push(t.oneOrNone('SELECT cohorts.id, cohorts.title, cohorts.slug, cohorts.start_date, cohorts.campus_id FROM ' +
                        'users_cohorts INNER JOIN cohorts ON users_cohorts.cohort_id = cohorts.id WHERE users_cohorts.user_id = $1 ORDER BY cohorts.start_date DESC LIMIT 1', user.id).then(cohort => {
                            userResult.cohort = cohort;
                    }));
                    queries.push(t.any('SELECT * FROM users_exams WHERE user_id =  $1',user.id).then(exams => {
                        userResult.exams = exams;
                    }));
                return(t.batch(queries))
            });
        }).then(result => {
            return res.json({
                code: 'S0003',
                status: 'success',
                message: 'Results retrieved successfully',
                data: userResult,
                origin: 'User Endpoint - Fetch Student',
                priority: 10
            })
        }).catch((err) => {
            console.log(err);
            return res.json({
                code: 'S0004',
                status: 'failure',
                message: 'An error has occurred.',
                data: err,
                origin: 'User endpoint - Fetch user',
                priority: 8
            });
        })
    }
});

module.exports = router;