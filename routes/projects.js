const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');

const db = require('../db').db;
const ft_string = require('../lib/ft_string');
const ft_token = require('../lib/ft_token');
const Project = require('../models/project');

// Fetch All projects
router.get('/', (req, res, next) => {
    const token = ft_token.verifyToken(req);
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'We could not verify your token.',
            origin: 'Projects endpoint - Fetch all projects',
            priority: 9
        })
    } else {
        let orderBy = 'projects.title';
        let page = 1;
        let limit = 20;
        let yieldValues = [];
        let returnValue = [];
        if (req.query.page) {
            page = parseInt(req.query.page);
        }
        if(req.query.results_per_page) {
            limit = parseInt(req.query.results_per_page);
        }
        if (req.query.order_by) {
            switch(req.query.order_by) {
                case 'projects.slug':
                    orderBy = req.query.order_by;
                    break;
                default: break;
            }
        }
        db.task('fetch-projects', function *(t) {
        	let projects = yield t.any('SELECT projects.id as project_id, projects.title as project_title, ' +
				'projects.description AS project_description, projects.pass_percentage, projects.slug AS ' +
				'project_slug, projects.size, modules.id AS module_id, modules.title AS module_title, ' +
				'modules.slug AS module_slug, modules.milestone_id FROM projects INNER JOIN modules ON ' +
				'projects.module_id = modules.id ORDER BY ' + orderBy + ' LIMIT ' + limit + 'OFFSET ' +
                limit * (page - 1));
        	let newProjectArray = [];
        	for (const project of projects) {
				let newProject = {
					id: project.project_id,
					slug: project.project_slug,
					title: project.project_title,
					passPercentage: project.pass_percentage,
					size: project.size,
					module: {
						id: project.module_id,
						title: project.module_title,
						slug: project.module_slug,
						milestoneID: project.milestone_id,
					}
				};
				newProjectArray.push(newProject);
			}
            return newProjectArray
        }).then(result => {
            return res.json({
                code: 'S0002',
                status: 'success',
                message: 'Results Retrieved Successfully',
                data: result,
                origin: 'Projects endpoint - Fetch all projects',
                priority: 10
            })
        }).catch(err => {
            return res.json({
                code: 'S0003',
                status: 'failure',
                message: 'An error has occurred.',
                data: err,
                origin: 'Projects endpoint - Fetch all projects',
                priority: 8
            })
        });
    }
});

router.get('/:id', (req, res, next) => {
    const token = ft_token.verifyToken(req);
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'We could not verify your token.',
            origin: 'Projects endpoint - Fetch single project',
            priority: 9
        })
    } else if (!req.params.id) {
        return res.json({
            code: 'S0002',
            status: 'failure',
            message: 'A project id is required.',
            origin: 'Projects endpoint - Fetch single project',
            priority: 9
        })
	} else {
    	let query = 'id = $1';
    	if (isNaN(req.params.id)) {
    		query = 'slug = $1';
		}
        db.task('fetch-project', t => {
            return t.oneOrNone('SELECT id, title, description, slug FROM projects WHERE ' + query, [req.params.id]);
        }).then(result => {
        	"use strict";
            if (result) {
                return res.json({
                    code: 'S0003',
                    status: 'success',
                    message: 'results retrieved successfully.',
                    data: result,
                    origin: 'Projects endpoint - Fetch single project',
                    priority: 10
                })
            } else {
                return res.json({
                    code: 'S0004',
                    status: 'failure',
                    message: 'That project does not exist.',
                    origin: 'Projects endpoint - Fetch single project',
                    priority: 9
                });
            }
		}).catch(error => {
            if (error) {
                return res.json({
                    code: 'S0005',
                    status: 'failure',
                    message: 'An error has occurred.',
                    data: error,
                    origin: 'Projects endpoint - New project',
                    priority: 8
                })
            }
		});
    }
});

router.post('/', (req, res, next) => {
    const token = ft_token.verifyToken(req);
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'We could not verify your token.',
            origin: 'Projects endpoint - New project',
            priority: 9
        })
    } else if (token.role !== 'administrator') {
        return res.json({
            code: 'S0002',
            status: 'failure',
            message: 'Access Denied.',
            origin: 'Projects endpoint - New project',
            priority: 9
        })
    } else {
        if (!req.body.title) {
            return res.json({
                code: 'S0003',
                status: 'failure',
                message: 'A project title is required. Please try again.',
                origin: 'Projects endpoint - New project',
                priority: 9
            })
        } else if (!req.body.passPercentage) {
            return res.json({
                code: 'S0004',
                status: 'failure',
                message: 'A project pass percentage is required. Please try again.',
                origin: 'Projects endpoint - New project',
                priority: 9
            })
		} else if (!req.body.moduleId) {
            return res.json({
                code: 'S0005',
                status: 'failure',
                message: 'A project ModuleId is required. Please try again.',
                origin: 'Projects endpoint - New project',
                priority: 9
            })
        } else if (!req.body.size) {
            return res.json({
                code: 'S0006',
                status: 'failure',
                message: 'A project size is required. Please try again.',
                origin: 'Projects endpoint - New project',
                priority: 9
            })
		} else {
            let project = new Project(req.body);
            let queryArray = [project.slug, project.title, project.passPercentage, project.size, project.description, project.moduleId];
            db.task('Add Module', t => {
                return t.none('INSERT INTO projects(slug, title, pass_percentage, size, description, module_id) ' +
					'VALUES($1, $2, $3, $4, $5, $6)ON CONFLICT (slug) DO NOTHING', queryArray);
            }).then(result => {
                return res.json({
                    code: 'S0004',
                    status: 'success',
                    message: 'Record added successfully.',
                    origin: 'Projects endpoint - New project',
                    priority: 10
                })
            }).catch( error => {
                if (error) {
                    return res.json({
                        code: 'S0005',
                        status: 'failure',
                        message: 'An error has occurred.',
                        data: error,
                        origin: 'Projects endpoint - New project',
                        priority: 8
                    })
                }
            });
        }
    }
});

router.get('/:id/instances', (req, res, next) => {
    const origin = 'Projects endpoint - fetch project instances';
    const token = ft_token.verifyToken(req);
    if (!token) {
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'We could not verify your token.',
            origin: origin,
            priority: 9
        })
    } else if (!req.params.id) {
        return res.json({
            code: 'S0002',
            status: 'failure',
            message: 'a project "id" parameter is required in the url of your request.',
            origin: origin,
            priority: 9
        })
	} else {
    	let query = '$1';
    	if (isNaN(req.params.id)) {
    		query = '(SELECT id from projects WHERE slug = $1)';
		}
        db.task(t => {
            return t.any('SELECT id, title, start_date, end_date, validation_grade from projects_instances WHERE project_id = ' + query, [req.params.id]);
        }).then(result => {
            return res.json({
                code: 'S0003',
                status: 'success',
                message: 'Records retrieved successfully.',
				data: result,
                origin: origin,
                priority: 10
            })
        }).catch( error => {
            if (error) {
                return res.json({
                    code: 'S0004',
                    status: 'failure',
                    message: 'An error has occurred.',
                    data: error,
                    origin: origin,
                    priority: 8
                })
            }
        });
    }
});

router.post('/:id/instances', (req, res, next) => {
	"use strict";
	const origin = 'Projects endpoint - New project instance';
    const token = ft_token.verifyToken(req);
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'We could not verify your token.',
			origin: origin,
            priority: 9
        })
    } else if (token.role !== 'administrator') {
        return res.json({
            code: 'S0002',
            status: 'failure',
            message: 'Access Denied.',
            origin: origin,
            priority: 9
        })
    } else if (!req.body.title) {
        return res.json({
            code: 'S0003',
            status: 'failure',
            message: 'A project instance required a title. Please try again.',
            origin: origin,
            priority: 9
        })
    } else if (!req.body.validationGrade) {
        return res.json({
            code: 'S0004',
            status: 'failure',
            message: 'A project instance requires a validation grade. Please try again.',
            origin: origin,
            priority: 9
        })
    } else if (isNaN(req.body.validationGrade)) {
        if (!req.body.startDate) {
            return res.json({
                code: 'S0004',
                status: 'failure',
                message: 'A project instance requires a start date. Please try again.',
                origin: origin,
                priority: 9
            })
        } else if (!req.body.endDate) {
            return res.json({
                code: 'S0005',
                status: 'failure',
                message: 'A project instance requires a closing date. Please try again.',
                origin: origin,
                priority: 9
            })
		} else {
            let query = 'id = $1';
            if (isNaN(req.params.id)) {
                query = 'title = $1';
            }
            return db.tx(t => {
                return t.oneOrNone('SELECT * from projects WHERE ' + query + ' LIMIT 1', req.params.id)
                    .then(project => {
                        if (!project) {
                            throw res.json({
                                code: 'S0006',
                                status: 'failure',
                                message: 'That project does not exist. Please try again.',
                                origin: origin,
                                priority: 9
                            })
                        }
                    })
                    .then(projectResult => {
                        t.oneOrNone('INSERT INTO projects_instances(title, project_id, start_date, end_date, validation_grade' +
                            'date_created, date_updated) VALUES($1, $2, $3, $4, $5, now(), now()) ON CONFLICT ON CONSTRAINT ' +
                            'uc_projects_instances DO NOTHING RETURNING id ', [req.body.title, req.params.id, req.body.startDate, req.body.endDate, req.body.validationGrade])
                            .then(instanceResult => {
                                if (instanceResult) {
                                    return res.json({
                                        code: 'S0007',
                                        status: 'success',
                                        message: 'Project instance added successfully.',
                                        origin: origin,
                                        priority: 9
                                    })
                                } else {
                                    throw res.json({
                                        code: 'S0008',
                                        status: 'failure',
                                        message: 'That project instance already exists. Please try again.',
                                        origin: origin,
                                        priority: 9
                                    })
                                }
                            })
                            .catch(error => {
                                throw res.json({
                                    code: 'S0009',
                                    status: 'failure',
                                    message: 'We could not add the requested project instance. Please try again.',
                                    origin: origin,
                                    priority: 9
                                })
                            })
                    })
            })
        }
    }
});

module.exports = router;