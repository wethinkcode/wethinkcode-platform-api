const express = require('express');
const router = express.Router();

const db = require('../db').db;
const ft_token = require('../lib/ft_token');
const ft_string = require('../lib/ft_string');

//fetch reporting data specific to a particular task
router.get('/', (req, res, next) => {
    "use strict";
    const token = ft_token.verifyToken(req);
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'We could not verify your token.',
            origin: 'Student Endpoint - Fetch user reporting data.',
            priority: 9
        })
    } else {
        let page = 1;
        let limit = 20;
        let year = new Date().getFullYear();
        let query = '';
        let queryParams = [];
        if (req.query.year) {
            if ( parseInt(req.query.year)) {
                year = parseInt(req.query.year);
                query = 'users_students.year = $1 AND ';
                queryParams.push(year);
            } else {
                return res.json({
                    code: 'S0002',
                    status: 'failure',
                    message: 'Could not parse \'page\' query parameter.',
                    origin: 'Student Endpoint - Fetch user reporting data.',
                    priority: 9
                })
            }
        }
        if (req.query.page) {
            if ( parseInt(req.query.page)) {
                page = parseInt(req.query.page);
            } else {
                return res.json({
                    code: 'S0003',
                    status: 'failure',
                    message: 'Could not parse \'page\' query parameter.',
                    origin: 'Student Endpoint -  Fetch user reporting data.',
                    priority: 9
                })
            }
        }
        if (req.query.results_per_page) {
            if ( parseInt(req.query.results_per_page)) {
                limit = parseInt(req.query.results_per_page);
            } else {
                return res.json({
                    code: 'S0004',
                    status: 'failure',
                    message: 'Could not parse \'results_per_page\' query parameter.',
                    origin: 'Student Endpoint - Fetch user reporting data.',
                    priority: 8
                })
            }
        }
        db.task('Fetch reporting data for users', function *(t) {
            let users = yield t.any('SELECT users.id, users.first_name, users.last_name, users.display_name, users.email, users.role, users.cover_image, users.username, users_students.year, users_students.state FROM users_students INNER JOIN users ON users_students.user_id = users.id WHERE ' + query +  'users.role = \'student\' ORDER BY users.username ASC LIMIT ' + limit + ' OFFSET ' + (limit * (page - 1)), queryParams);
            for (const user of users) {
                user.exams = yield t.any('SELECT exam_id, grade FROM users_exams WHERE user_id = $1', [user.id]);
                user.attendance = yield t.any('SELECT DISTINCT ON (date(access_date)) access_date FROM users_attendance WHERE access_date > NOW() - INTERVAL \'120 days\' AND user_id = $1 ORDER BY date(access_date) DESC', [user.id]);
                user.projects = yield t.any('SELECT projects.id, projects.title, projects.pass_percentage, projects.size, projects.module_id, users_projects.final_mark, projects.slug FROM users_projects INNER JOIN projects ON users_projects.project_id = projects.id WHERE user_id = $1', [user.id]);
            }
            return users;
        }).then(result => {
            return res.json({
                code: 'S0005',
                status: 'success',
                message: 'Results found.',
                data: result,
                origin: 'Student Endpoint - Fetch user reporting data.',
                priority: 10
            })
        }).catch((err) => {
            console.log(err);
            return res.json({
                code: 'S0006',
                status: 'failure',
                message: 'An error has occurred.',
                data: err,
                origin: 'Student Endpoint - Fetch user reporting data.',
                priority: 8
            })
        })
    }
});

module.exports = router;
