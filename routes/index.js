var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.redirect('/documentation/v' + process.env.CURRENT_DOCUMENTATION_VERSION + '/');
});

module.exports = router;
