const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const postmark = require("postmark");

const db = require('../db').db;
const ft_string = require('../lib/ft_string');
const ft_token = require('../lib/ft_token');

const SALT_WORK_FACTOR = 10;
const emailRegex = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);

//User creation Endpoint
router.post('/', (req, res, next) => {
    const origin = 'User Endpoint - User creation';
    if (!req.body.firstName){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'A \'firstName\' variable is required in the body of your request.',
            origin: origin,
            priority: 9
        })
    } else if (!req.body.lastName) {
        return res.json({
            code: 'S0002',
            status: 'failure',
            message: 'A \'lastName\' variable is required in the body of your request.',
            origin: origin,
            priority: 9
        })

    } else if (!req.body.email) {
        return res.json({
            code: 'S0003',
            status: 'failure',
            message: 'An \'email\' variable is required in the body of your request.',
            origin: origin,
            priority: 9
        })
    } else if (!emailRegex.test(req.body.email)) {
        return res.json({
            code: 'S0004',
            status: 'failure',
            message:'An invalid email address has been provided.',
            origin: origin,
            priority: 9
        })
    } else if (!req.body.password) {
        return res.json({
            code: 'S0003',
            status: 'failure',
            message:'A "\'password\' variable is required in the body of your request.',
            origin: origin,
            priority: 9
        })
    } else if (req.body.password.length < 6) {
        return res.json({
            code: 'S0004',
            status: 'failure',
            message:'Your password should be at least 6 characters long.',
            origin: origin,
            priority: 9
        })
    } else if (!req.body.passwordRepeat || req.body.passwordRepeat !== req.body.password) {
        return res.json({
            code: 'S0005',
            status: 'failure',
            message: '\'password\' and \'passwordRepeat\' variables do not match.',
            origin: origin,
            priority: 9
        })
    } else {
        db.any('SELECT * FROM users WHERE email = $1', [req.body.email], [true])
            .then(function (data) {
                if (data.length > 0) {
                    return res.json({
                        code: 'S0006',
                        status: 'failure',
                        message: req.body.email + ' already exists. Please try again',
                        origin: origin,
                        priority: 9
                    })
                } else {
                    bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
                        if (err) {
                            return res.json({
                                code: 'S0007',
                                status: 'failure',
                                message: 'An error has occurred.',
                                data: err,
                                origin: origin,
                                priority: 8
                            })
                        } else {
                            bcrypt.hash(req.body.password, salt, function (err, hash) {
                                if (err) {
                                    return res.json({
                                        code: 'S0008',
                                        status: 'failure',
                                        message: 'An error has occurred.',
                                        data: err,
                                        origin: origin,
                                        priority: 8
                                    })
                                } else {
                                    const firstName = req.body.firstName;
                                    const lastName = req.body.lastName;
                                    const email = req.body.email;
                                    const role = 'applicant';
                                    const values = [firstName, lastName, email, hash, role];
                                    db.one('INSERT INTO users(first_name, last_name, email, password, role) VALUES($1, $2, $3, $4, $5) RETURNING id', values).then(data => {
                                        if(data.id) {
                                            const signup_token =  jwt.sign({string: ft_string.randomString(15), id: data.id}, process.env.JWT_SECRET);
                                            db.any('SELECT id FROM tools WHERE type=\'applicant\' OR type=\'global\'').then(results => {
                                                "use strict";
                                                if (results) {
                                                    let queryString = '';
                                                    let counter = 1;
                                                    for(const result of results) {
                                                        queryString += '(' + result.id + ',' + data.id + ')';
                                                        if (counter !== results.length) {
                                                            queryString += ','
                                                        }
                                                        counter++;
                                                    }
                                                    db.none('INSERT INTO users_tools(tool_id, user_id) VALUES' + queryString);
                                                }
                                            });
                                            db.none('UPDATE users SET signup_token=$1 WHERE id=$2', [signup_token, data.id]);
                                            const client = new postmark.Client(process.env.POSTMARK_API_KEY);
                                            client.sendEmailWithTemplate({
                                                "From": 'support@wethinkcode.co.za',
                                                "To": email,
                                                "TemplateId": 4217205,
                                                "TemplateModel": {
                                                    "first_name":  firstName,
                                                    "activation_url": process.env.HOST + '/signup?activate=true&token=' + signup_token + '&id=' + data.id,
                                                }
                                            }, function(emailSendingError, result) {
                                                if (emailSendingError) {
                                                    throw res.json({
                                                        code: 'S0009',
                                                        status: 'failure',
                                                        message: 'An error has occurred.',
                                                        data: error,
                                                        origin: origin,
                                                        priority: 8
                                                    })
                                                } else {
                                                    return res.json({
                                                        code: 'S0010',
                                                        status: 'success',
                                                        message: 'User Created Successfully.',
                                                        origin: origin,
                                                        priority: 10
                                                    });
                                                }
                                            });
                                        } else {
                                            return res.json({
                                                code: 'S0011',
                                                status: 'failure',
                                                message: 'We could not add that user. Please try again.',
                                                data: error,
                                                origin: origin,
                                                priority: 9
                                            })
                                        }
                                    })
                                        .catch(error => {
                                            return res.json({
                                                code: 'S0012',
                                                status: 'failure',
                                                message: 'An error has occurred.',
                                                data: error,
                                                origin: origin,
                                                priority: 8
                                            })
                                        });
                                }
                            });
                        }
                    });
                }
            })
            .catch(function (error) {
                return res.json({
                    code: 'S0013',
                    status: 'failure',
                    message: 'An error has occurred.',
                    data: error,
                    origin: origin,
                    priority: 8
                })
            });
    }
});

//Get all users
router.get('/', (req, res, next) => {
    "use strict";
    let token = ft_token.verifyToken(req);
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'Could not verify your webToken. Please contact support. (Code: S0001)',
            origin: 'User Endpoint - Fetch Users',
            priority: 9
        })
    } else {
        if (token.role === 'applicant') {
            return res.json({
                code: 'S0002',
                status: 'failure',
                message: 'Access denied.',
                origin: 'User Endpoint -  Fetch Users',
                priority: 9
            })
        } else {
            let page = 1;
            let limit = 20;
            let search = '';
            if (req.query.page) {
                if ( parseInt(req.query.page)) {
                    page = parseInt(req.query.page);
                } else {
                    return res.json({
                        code: 'S0003',
                        status: 'failure',
                        message: 'Could not parse \'page\' query parameter.',
                        origin: 'User Endpoint - Fetch Users',
                        priority: 9
                    })
                }
            }
            if (req.query.results_per_page) {
                if ( parseInt(req.query.results_per_page)) {
                    limit = parseInt(req.query.results_per_page);
                } else {
                    return res.json({
                        code: 'S0004',
                        status: 'failure',
                        message: 'Could not parse \'results_per_page\' query parameter.',
                        origin: 'User Endpoint - Fetch Users',
                        priority: 8
                    })
                }
            }
            if (req.query.search) {
                search = req.query.search;
            }
            db.any('SELECT id, first_name, last_name, display_name, email, role, cover_image, username FROM users WHERE username ~* $1 OR first_name ~* $1 OR last_name ~* $1 OR first_name || \' \' || last_name ~* $1 LIMIT ' + limit + ' OFFSET ' +
                (limit * (page - 1)), [search]).then(users => {
                return res.json({
                    code: 'S0005',
                    status: 'success',
                    message: 'Results Retrieved Successfully',
                    data: users,
                    origin: 'User Endpoint - Fetch Users',
                    priority: 10
                })
            }).catch((err) => {
                console.log(err);
                return res.json({
                    code: 'S0006',
                    status: 'failure',
                    message: 'An error has occurred.',
                    data: err,
                    origin: 'User Endpoint - Fetch Users',
                    priority: 8
                })
            })
        }

    }
});

// User authentication verification
router.get('/auth', (req, res, next) => {
    "use strict";
    const token = ft_token.verifyToken(req);
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'We could not verify your token.',
            origin: 'User Endpoint - User Authentication (Verification)',
            priority: 9
        })
    } else {
        return res.json({
            code: 'S0002',
            status: 'success',
            message: 'Token verified successfully.',
            data: {
                token: token,
            },
            origin: 'User Endpoint - User Authentication (Verification)',
            priority: 10
        })
    }
});

// User authentication
router.post('/auth', (req, res, next) => {
    if (!req.body.identifier){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'An "identifier" variable is required in the body of your request.',
            origin: 'User Endpoint - User Authentication',
            priority: 9
        })
    }  else if (!req.body.password){
        return res.json({
            code: 'S0002',
            status: 'failure',
            message: 'A "password" variable is required in the body of your request.',
            origin: 'User Endpoint - User Authentication',
            priority: 9
        })
    } else {
        db.any('SELECT * FROM users WHERE email = $1 OR username = $1', [req.body.identifier], [true])
            .then(function (data) {
                if (!data.length) {
                    return res.json({
                        code: 'S0003',
                        status: 'failure',
                        message: 'That user does not exist. Please try again or Sign up to continue.',
                        origin: 'User Endpoint - User Authentication',
                        priority: 9
                    })
                } else {
                    if (data[0].activated !== true) {
                        const signup_token =  jwt.sign({string: ft_string.randomString(15), id: data[0].id}, process.env.JWT_SECRET);
                        db.none('UPDATE users SET signup_token=$1 WHERE id=$2', [signup_token, data[0].id]);
                        const sg = require('sendgrid')(process.env.SENDGRID_API_KEY);
                        let request = sg.emptyRequest({
                            method: 'POST',
                            path: '/v3/mail/send',
                            body: {
                                personalizations: [
                                    {
                                        to: [
                                            {
                                                'email': data[0].email,
                                            },
                                        ],
                                        'substitutions': {
                                            '-activation-': process.env.HOST + '/signup?activate=true&token=' + signup_token + '&id=' + data[0].id,
                                            '-name-': data[0].first_name,
                                        },
                                        subject: 'Account activation',
                                    },
                                ],
                                from: {
                                    email: 'accounts@wethinkcode.co.za',
                                    name: 'WeThinkCode_ Accounts'
                                },
                                content: [
                                    {
                                        type: 'text/html',
                                        value: '<p></p>',
                                    },
                                ],
                                'template_id': process.env.SENDGRID_SIGNUP_TEMPLATE,
                            },
                        });
                        sg.API(request, function (error, response) {
                            if (error) {
                                return res.json({
                                    code: 'S0004',
                                    status: 'failure',
                                    message: 'An error has occurred.',
                                    data: error,
                                    origin: 'User Endpoint - User Authentication',
                                    priority: 8
                                })
                            } else {
                                return res.json({
                                    code: 'S0005',
                                    status: 'failure',
                                    message: 'Your account has not been activated yet. Check your email to continue.',
                                    origin: 'User Endpoint - User Authentication',
                                    priority: 9
                                })
                            }
                        });
                    } else {
                        bcrypt.compare(req.body.password, data[0].password, function(err, isValid) {
                            if (err) {
                                return res.json({
                                    code: 'S0006',
                                    status: 'failure',
                                    message: 'We could not complete that task. Please contact support. (Code: S0006)',
                                    origin: 'User Endpoint - User Authentication',
                                    priority: 8
                                })
                            } else if (!isValid){
                                return res.json({
                                    code: 'S0007',
                                    status: 'failure',
                                    message: 'Your email or password is incorrect. Please try again.',
                                    origin: 'User Endpoint - User Authentication',
                                    priority: 9
                                })
                            } else {
                                db.none('UPDATE users SET last_signin=NOW() WHERE id=$1', [data[0].id]);
                                return res.json({
                                    code: 'S0008',
                                    status: 'success',
                                    message: 'Signin success.',
                                    origin: 'User Endpoint - User Authentication',
                                    data: {
                                        token: jwt.sign({
                                            id: data[0].id,
                                            role: data[0].role,
                                        }, process.env.JWT_SECRET)
                                    },
                                    priority: 10
                                })
                            }
                        });
                    }
                }
            })
            .catch(function (error) {
                return res.json({
                    code: 'S0009',
                    status: 'failure',
                    message: 'An error has occurred.',
                    data: error,
                    origin: 'User Endpoint - User creation',
                    priority: 8
                })
            });
    }
});

//Retrieve all leave
router.get('/leave', (req, res, next) => {
    "use strict";
    let page = 1;
    let limit = 20;
    const origin = 'User Endpoint - Get leave';
    let token = ft_token.verifyToken(req);
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'Could not verify your webToken. Please contact support. (Code: S0001)',
            origin: origin,
            priority: 8
        })
    } else if (token.role !== 'administrator') {
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'Access Denied.',
            origin: origin,
            priority: 8
        })
    } else {
        db.any('SELECT * FROM users_leave LIMIT ' + limit + ' OFFSET ' + (limit * (page - 1))).then(leave => {
            return res.json({
                code: 'S0006',
                status: 'success',
                data: leave,
                message: 'Records found.',
                origin: origin,
                priority: 10
            })
        }).catch(error => {
            console.log(error);
            return res.json({
                code: 'S0007',
                status: 'failure',
                message: 'An error has occurred.',
                data: error,
                origin: origin,
                priority: 8
            })
        })
    }
});

//Get a single user
router.get('/:id', (req, res, next) => {
    let token = ft_token.verifyToken(req);
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'Could not verify your webToken. Please contact support. (Code: S0001)',
            origin: 'User Endpoint - User update',
            priority: 8
        })
    } else if (!req.params.id){
        return res.json({
            code: 'S0002',
            status: 'failure',
            message: 'An "id" query parameter is required. Please try again.',
            origin: 'User Endpoint - User update',
            priority: 9
        })
    } else {
        let query = 'id = $1';
        if (!parseInt(req.params.id)) {
            query = 'username = $1';
        }
        db.oneOrNone('SELECT id, first_name, last_name, display_name, email, role, profile_image, cover_image, username FROM users WHERE ' + query, [req.params.id]).then(user => {
            return res.json({
                code: 'S0003',
                status: 'success',
                message: 'Results Retrieved Successfully',
                data: user,
                origin: 'User Endpoint - Fetch User',
                priority: 10
            })
        }).catch((err) => {
            console.log(err);
            return res.json({
                code: 'S0004',
                status: 'failure',
                message: 'An error has occurred.',
                data: err,
                origin: 'User Endpoint - Fetch User',
                priority: 8
            });
        })
    }
});

//User information
router.get('/:id/information', (req, res, next) => {
    let token = ft_token.verifyToken(req);
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'Could not verify your webToken. Please contact support. (Code: S0001)',
            origin: 'User Endpoint - User update',
            priority: 8
        })
    } else if (!req.params.id){
        return res.json({
            code: 'S0002',
            status: 'failure',
            message: 'An "id" query parameter is required. Please try again.',
            origin: 'User Endpoint - User update',
            priority: 9
        })
    } else {
        let query = 'users.id = $1';
        if (!parseInt(req.params.id)) {
            query = 'users.username = $1';
        }
        db.oneOrNone('SELECT users.first_name, users.last_name, users.display_name, users.email, users.role, ' +
            'users.profile_image, users.cover_image, users.username, users_information.birth_date, ' +
            'users_information.phone, users_information.gender, users_information.zip_code, users_information.country, ' +
            'users_information.birth_city, users_information.birth_country, users_information.postal_street, ' +
            'users_information.citizenship_country, users_information.computer_access, ' +
            'users_information.code_experience, users_information.internet_access, users_information.accommodation, ' +
            'users_information.preferred_name, users_information.id_number, users_information.ethnicity, ' +
            'users_information.home_language, users_information.marital_status, users_information.home_town, ' +
            'users_information.home_province, users_information.high_school, users_information.education, ' +
            'users_information.medical_aid_name, users_information.medical_aid_number, users_information.referral, ' +
            'users_information.campus FROM users INNER JOIN users_information ON ' +
            'users.id = users_information.user_id WHERE ' + query, [req.params.id]).then(user => {
            if (token.role === 'staff' || token.role === 'administrator') {
                return res.json({
                    code: 'S0003',
                    status: 'success',
                    message: 'Results Retrieved Successfully',
                    data: user,
                    origin: 'User Endpoint - Fetch User',
                    priority: 8
                })
            }
            let result = {};
            if (user.phone)
                result.phone = user.phone;
            return res.json({
                code: 'S0003',
                status: 'success',
                message: 'Results Retrieved Successfully',
                data: result,
                origin: 'User Endpoint - Fetch User',
                priority: 8
            })
        }).catch((err) => {
            console.log(err);
            return res.json({
                code: 'S0004',
                status: 'failure',
                message: 'An error has occurred.',
                data: err,
                origin: 'User Endpoint - Fetch User',
                priority: 8
            });
        })
    }
});

//get leave for a user
router.get('/:id/leave', (req, res, next) => {
    "use strict";
    let page = 1;
    let limit = 20;
    const origin = 'User Endpoint - Get user leave';
    let token = ft_token.verifyToken(req);
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'Could not verify your webToken. Please contact support. (Code: S0001)',
            origin: origin,
            priority: 8
        })
    } else if (token.role !== 'administrator') {
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'Access Denied.',
            origin: origin,
            priority: 8
        })
    } else if (!req.params.id){
        return res.json({
            code: 'S0002',
            status: 'failure',
            message: 'An "id" query parameter is required. Please try again.',
            origin: origin,
            priority: 9
        })
    } else {
        let query = 'user_id = $1';
        if (!parseInt(req.params.id)) {
            query = 'user_id = (SELECT id FROM users WHERE username = $1)';
        }
        db.any('SELECT start_date, end_date, reason, approved FROM users_leave WHERE ' + query + ' LIMIT '  + limit +
            ' OFFSET ' + (limit * (page - 1)), req.params.id).then(leave => {
            return res.json({
                code: 'S0006',
                status: 'success',
                data: leave,
                message: 'Records found.',
                origin: origin,
                priority: 10
            })
        }).catch(error => {
            console.log(error);
            return res.json({
                code: 'S0007',
                status: 'failure',
                message: 'An error has occurred.',
                data: error,
                origin: origin,
                priority: 8
            })
        })
    }
});

router.post('/invite', (req, res, next) => {
    const origin = 'User endpoint - User Invitation';
    let token = ft_token.verifyToken(req);
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'Could not verify your webToken. Please contact support. (Code: S0001)',
            origin: origin,
            priority: 8
        })
    } else if (!req.body.firstName){
        return res.json({
            code: 'S0002',
            status: 'failure',
            message: 'A \'firstName\' variable is required in the body of your request.',
            origin: origin,
            priority: 9
        })
    } else if (!req.body.lastName) {
        return res.json({
            code: 'S0003',
            status: 'failure',
            message: 'A \'lastName\' variable is required in the body of your request.',
            origin: origin,
            priority: 9
        })

    } else if (!req.body.username) {
        return res.json({
            code: 'S0004',
            status: 'failure',
            message: 'A \'username\' variable is required in the body of your request.',
            origin: origin,
            priority: 9
        })

    } else if (!req.body.email) {
        return res.json({
            code: 'S0005',
            status: 'failure',
            message: 'An \'email\' variable is required in the body of your request.',
            origin: origin,
            priority: 9
        })
    } else if (!emailRegex.test(req.body.email)) {
        return res.json({
            code: 'S0006',
            status: 'failure',
            message: 'An invalid email address has been provided.',
            origin: origin,
            priority: 9
        })
    } else if (!req.body.role) {
        return res.json({
            code: 'S0007',
            status: 'failure',
            message: 'A \'role\' variable is required in the body of your request.',
            origin: origin,
            priority: 9
        })
    } else {
        let newUser = {};
        newUser.password = ft_string.randomString(15);
        newUser.username = req.body.username;
        newUser.firstName = req.body.firstName;
        newUser.lastName = req.body.lastName;
        newUser.email = req.body.email;
        newUser.role = req.body.role;
        bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
            if (err) {
                return (res.json({
                    code: 'S0008',
                    status: 'failure',
                    message: 'An error has occurred.',
                    data: err,
                    origin: origin,
                    priority: 8
                }));
            } else {
                bcrypt.hash(newUser.password, salt).then(hashResult => {
                    db.task(t => {
                        "use strict";
                        return t.any('SELECT * FROM users WHERE username = $1 OR email = $2', [req.body.username, req.body.email])
                            .then(user => {
                                if (user.length) {
                                   throw res.json({
                                        code: 'S0009',
                                        status: 'failure',
                                        message: 'That user already exists.',
                                        origin: origin,
                                        priority: 9
                                    });
                                } else {
                                    const query = 'INSERT INTO users(first_name, last_name, username, email, role, password, activated) ' +
                                        'VALUES($1, $2, $3, $4, $5, $6, true) RETURNING id';
                                    const queryArray = [newUser.firstName, newUser.lastName, newUser.username, newUser.email, newUser.role, hashResult];
                                    return t.oneOrNone(query, queryArray).catch(errorAddingUser => {
                                        throw res.json({
                                            code: 'S0010',
                                            status: 'failure',
                                            message: 'Could not add user to our records.',
                                            error: error,
                                            origin: origin,
                                            priority: 8
                                        });
                                    });
                                }
                            })
                            .catch(error => {
                                console.log(error);
                                throw res.json({
                                    code: 'S0011',
                                    status: 'failure',
                                    message: 'Could not check if user exists in our records.',
                                    error: error,
                                    origin: origin,
                                    priority: 8
                                });
                            })
                            .then(userAddingResult => {
                                const queries = [];
                                newUser.id = userAddingResult.id;
                                for (const tool of req.body.tools) {
                                    queries.push(t.none('INSERT INTO users_tools(user_id, tool_id) VALUES($1, $2)', [newUser.id, tool.id]));
                                    //queries.push(t.none('INSERT INTO users_information(user_id) VALUES($1)', [newUser.id]))
                                }
                                return t.batch(queries);
                            })
                            .catch(error => {
                                throw res.json({
                                    code: 'S0012',
                                    status: 'failure',
                                    message: 'Could not add user tools to our records.',
                                    origin: origin,
                                    priority: 8
                                });
                            })
                            .then(batchAddingToolsResult => {
                                return t.one('SELECT * FROM users WHERE id = $1', token.id);
                            })
                            .catch(error => {
                                throw res.json({
                                    code: 'S0013',
                                    status: 'failure',
                                    message: 'Could find requesting user details in our records.',
                                    origin: origin,
                                    priority: 8
                                });
                            })
                            .then(requestingUser => {
                                const client = new postmark.Client(process.env.POSTMARK_API_KEY);
                                client.sendEmailWithTemplate({
                                    "From": "support@wethinkcode.co.za",
                                    "To": newUser.email,
                                    "TemplateId": 4217209,
                                    "TemplateModel": {
                                        "first_name": newUser.first_name,
                                        "inviting_user": requestingUser.first_name,
                                        "username": newUser.username,
                                        "password": newUser.password,
                                        "activation_url": process.env.HOST + '/signin',
                                    }
                                }, function(emailSendingError, result) {
                                    console.log(emailSendingError);
                                    console.log(result);
                                    if (emailSendingError) {
                                        throw res.json({
                                            code: 'S0015',
                                            status: 'failure',
                                            message: 'An error has occurred.',
                                            data: emailSendingError,
                                            origin: origin,
                                            priority: 8
                                        })
                                    } else {
                                        return res.json({
                                            code: 'S0014',
                                            status: 'success',
                                            message: 'User Invited Successfully.',
                                            origin: origin,
                                            priority: 10
                                        });
                                    }
                                });
                            })
                            .catch(hashError => {
                                throw res.json({
                                    code: 'S0016',
                                    status: 'failure',
                                    message: 'An error has occurred.',
                                    data: hashError,
                                    origin: origin,
                                    priority: 8
                                })
                            });
                    });
                })
            }
        });
    }
});

// Activating a user account
router.patch('/auth/activate/:id', (req, res, next) => {
    let token = ft_token.verifyToken(req);
    if (!token){
        return res.json({
            code: 'S0001',
	        status: 'failure',
            message: 'Could not verify your webToken. Please contact support. (Code: S0001)',
            origin: 'User Endpoint - User account activation',
            priority: 8
        })
    } else if (!req.params.id){
        return res.json({
            code: 'S0002',
	        status: 'failure',
            message: 'An "id" query parameter is required. Please try again.',
            origin: 'User Endpoint - User account activation',
            priority: 9
        })
    } else {
        db.any('SELECT * FROM users WHERE id = $1', [req.params.id], [true])
            .then(function (data) {
                "use strict";
                if (!data.length) {
                    return res.json({
                        code: 'S0003',
	                    status: 'failure',
                        message: 'That user does not exist.',
                        origin: 'User Endpoint - User account activation',
                        priority: 9
                    })
                } else {
                    if (token.id !== data[0].id && data[0].role !== 'administrator'){
                        return res.json({
                            code: 'S0004',
	                        status: 'failure',
                            message: 'Access denied.',
                            origin: 'User Endpoint - User account activation',
                            priority: 9
                        })
                    } else {
                        if (data[0].activated === true){
                            return res.json({
                                code: 'S0005',
	                            status: 'failure',
                                message: 'Your account has already been activated. Sign in to continue.',
                                origin: 'User Endpoint - User account activation',
                                priority: 9
                            })
                        } else if (req.headers["authorization"].split(" ")[1] !== data[0].signup_token) {
                            return res.json({
                                code: 'S0006',
	                            status: 'failure',
                                message: 'Your token is invalid.',
                                origin: 'User Endpoint - User account activation',
                                priority: 8
                            })
                        } else  {
                            db.one('UPDATE users SET activated=true, signup_token=NULL WHERE id = $1 RETURNING id', [req.params.id], [true])
                                .then(function (data) {
                                    if (!data.id) {
                                        return res.json({
                                            code: 'S0007',
	                                        status: 'failure',
                                            message: 'We Could not update that user. Please try again.',
                                            origin: 'User Endpoint - User account activation',
                                            priority: 8
                                        })
                                    } else {
                                        return res.json({
                                            code: 'S0008',
	                                        status: 'success',
                                            message: 'User updated successfully.',
                                            origin: 'User Endpoint - User account activation',
                                            priority: 10
                                        })
                                    }
                                }).catch(error => {
                                    return res.json({
                                        code: 'S0009',
                                        status: 'failure',
                                        message: 'An error has occurred.',
                                        data: error,
                                        origin: 'User Endpoint - User account activation',
                                        priority: 8
                                    })
                                });
                        }
                    }
                }
            }).catch(error => {
                return res.json({
                    code: 'S0010',
                    status: 'failure',
                    message: 'An error has occurred.',
                    data: error,
                    origin: 'User Endpoint - User account activation',
                    priority: 8
                })
            });
    }
});

// User password reset process initialization
router.post('/auth/reset', (req, res, next) => {
    const origin = 'User Endpoint - User password reset init';
    if (!req.body.identifier) {
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'An \'identifier\' variable is required in the body of your request.',
	        origin: origin,
            priority: 9
        })
    } else {
        db.any('SELECT * FROM users WHERE email = $1 OR username = $1', [req.body.identifier], [true])
            .then(function (data) {
                "use strict";
                if (!data.length) {
                    return res.json({
                        code: 'S0003',
                        status: 'That user does not exist.',
                        origin: origin,
	                    priority: 9
                    })
                } else {
                    if (data[0].activated !== true) {
                        const signup_token =  jwt.sign({string: ft_string.randomString(15), id: data[0].id}, process.env.JWT_SECRET);
                        db.none('UPDATE users SET signup_token=$1 WHERE id=$2', [signup_token, data[0].id]);
                        const client = new postmark.Client(process.env.POSTMARK_API_KEY);
                        client.sendEmailWithTemplate({
                            "From": 'support@wethinkcode.co.za',
                            "To": data[0].email,
                            "TemplateId": 4217205,
                            "TemplateModel": {
                                "first_name":  data[0].first_name,
                                "activation_url": process.env.HOST + '/signup?activate=true&token=' + signup_token + '&id=' + data[0].id,
                            }
                        }, function(emailSendingError, result) {
                            if (emailSendingError) {
                                return res.json({
                                    code: 'S0004',
                                    status: 'failure',
                                    message: 'An error has occurred.',
                                    data: error,
                                    origin: origin,
                                    priority: 8
                                })
                            } else {
                                return res.json({
                                    code: 'S0005',
                                    status: 'failure',
                                    message: 'Your account has not been activated yet. Check your email to continue.',
                                    origin: origin,
                                    priority: 9
                                })
                            }
                        });
                    } else  {
                        const reset_token =  jwt.sign({string: ft_string.randomString(15), id: data[0].id}, process.env.JWT_SECRET);
                        db.none('UPDATE users SET reset_token=$1 WHERE id=$2', [reset_token, data[0].id]);

                        const client = new postmark.Client(process.env.POSTMARK_API_KEY);
                        client.sendEmailWithTemplate({
                            "From": 'support@wethinkcode.co.za',
                            "To": data[0].email,
                            "TemplateId": 4217421,
                            "TemplateModel": {
                                "first_name":  data[0].first_name,
                                "activation_url": process.env.HOST + '/signin?reset=true&token=' + reset_token + '&id=' + data[0].id,
                            }
                        }, function(emailSendingError, result) {
                            if (emailSendingError) {
                                return res.json({
                                    code: 'S0006',
                                    status: 'failure',
                                    message: 'An error has occurred.',
                                    data: error,
                                    origin: origin,
                                    priority: 8
                                })
                            } else {
                                return res.json({
                                    code: 'S0007',
                                    status: 'success',
                                    message: 'Password reset process started successfully. Check your email to continue.',
                                    origin: origin,
                                    priority: 9
                                })
                            }
                        });
                    }
                }
            }).catch(error => {
            return res.json({
                code: 'S0008',
                status: 'failure',
                message: 'An error has occurred.',
                data: error,
                origin: origin,
                priority: 8
            })
        });
    }
});

// User password reset_token validation
router.get('/auth/reset/:id', (req, res, next) => {
	"use strict";
	let token = ft_token.verifyToken(req);
	if (!token){
		return res.json({
			code: 'S0001',
			status: 'failure',
			message: 'Could not verify your webToken. Please contact support. (Code: S0001)',
			origin: 'User Endpoint - User password reset token verification',
			priority: 8
		})
	} else if (!req.params.id){
		return res.json({
			code: 'S0002',
			status: 'failure',
			message: 'An "id" query parameter is required. Please try again.',
			origin: 'User Endpoint - User password reset token verification',
			priority: 9
		})
	} else {
		db.any('SELECT id, reset_token FROM users WHERE id = $1', [req.params.id], [true])
			.then(function (data) {
				"use strict";
				if (!data.length) {
					return res.json({
						code: 'S0003',
						status: 'failure',
						message: 'That user does not exist.',
						origin: 'User Endpoint - User password reset token verification',
						priority: 9
					})
				} else if (data[0].reset_token !== req.headers["authorization"].split(" ")[1]){
					return res.json({
						code: 'S0004',
						status: 'failure',
						message: 'Your token is invalid.',
						origin: 'User Endpoint - User password reset token verification',
						priority: 9
					})
                } else {
					return res.json({
						code: 'S0005',
						status: 'success',
						message: 'You have provided a valid reset token.',
						origin: 'User Endpoint - User password reset token verification',
						priority: 10
					})
				}
			});
	}
});

//User password reset
router.patch('/auth/reset/:id', (req, res, next) => {
    "use strict";
	let token = ft_token.verifyToken(req);
	if (!token){
		return res.json({
			code: 'S0001',
			status: 'failure',
			message: 'Could not verify your webToken. Please contact support. (Code: S0001)',
			origin: 'User Endpoint - User password reset',
			priority: 8
		})
	} else if (!req.params.id){
		return res.json({
			code: 'S0002',
			status: 'failure',
			message: 'An "id" query parameter is required. Please try again.',
			origin: 'User Endpoint - User password reset',
			priority: 9
		})
	} else if (!req.body.password) {
		return res.json({
			code: 'S0003',
			status: 'failure',
			message:'A "\'password\' variable is required in the body of your request.',
			origin: 'User Endpoint - User password reset',
			priority: 9
		})
	} else if (req.body.password.length < 6) {
		return res.json({
			code: 'S0004',
			status: 'failure',
			message:'Your password should be at least 6 characters long.',
			origin: 'User Endpoint - User password reset',
			priority: 9
		})
	} else if (!req.body.passwordRepeat || req.body.passwordRepeat !== req.body.password) {
		return res.json({
			code: 'S0005',
			status: 'failure',
			message: '\'password\' and \'passwordRepeat\' variables do not match.',
			origin: 'User Endpoint - User password reset',
			priority: 9
		})
	} else {
		db.any('SELECT * FROM users WHERE id = $1', [req.params.id], [true])
			.then(function (data) {
				if (!data.length) {
					throw res.json({
						code: 'S0006',
						status: 'failure',
						message: 'That user does not exist. Please try again',
						origin: 'User Endpoint - User password reset',
						priority: 9
					})
				} else if (req.headers["authorization"].split(" ")[1] !== data[0].reset_token) {
					throw res.json({
						code: 'S0007',
						status: 'failure',
						message: 'Your token is invalid. Please contact support (Code: S0007)',
						origin: 'User Endpoint - User password reset',
						priority: 9
					})
				} else {
					bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
						if (err) {
							throw res.json({
								code: 'S0008',
								status: 'failure',
								message: 'An error has occurred.',
								data: err,
								origin: 'User Endpoint - User password reset',
								priority: 8
							})
						} else {
							bcrypt.hash(req.body.password, salt, function (err, hash) {
								if (err) {
									throw res.json({
										code: 'S0009',
										status: 'failure',
										message: 'An error has occurred.',
										data: err,
										origin: 'User Endpoint - User password reset',
										priority: 8
									})
								} else {
									db.any('UPDATE users SET password = $1, reset_token = NULL WHERE id = $2 RETURNING id', [ hash, data[0].id ])
										.then(data => {
										    if (data[0].id){
											    return res.json({
												    code: 'S0010',
												    status: 'success',
												    message: 'Password reset successfully.',
												    origin: 'User Endpoint - User password reset',
												    priority: 10
											    })
                                            } else {
											    throw res.json({
												    code: 'S0011',
												    status: 'failure',
												    message: 'We could not update that user. Please try again.',
												    origin: 'User Endpoint - User password reset',
												    priority: 8
											    })
                                            }
										})
								}
							});
						}
					});
				}
			})
			.catch(function (error) {
				return error
			});
	}
});

//User quick links
router.get('/:id/quicklinks', (req, res, next ) => {
    const token = ft_token.verifyToken(req);
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'We could not verify your token.',
            origin: 'User Endpoint - Fetch user quicklinks',
            priority: 9
        })
    } else {
        if (isNaN(req.params.id)) {
            db.task(t => {
                return t.oneOrNone('SELECT * FROM users WHERE username = $1', req.params.id).then(user => {
                    if (!user) {
                        return res.json({
                            code: 'S0002',
                            status: 'failure',
                            message: 'No users found.',
                            origin: 'User Endpoint - Fetch user quicklinks',
                            priority: 9
                        })
                    } else if (user.id !== token.id) {
                        return res.json({
                            code: 'S0003',
                            status: 'failure',
                            message: 'Access denied.',
                            origin: 'User Endpoint - Fetch user quicklinks',
                            priority: 9
                        })
                    } else {
                        let query = 'SELECT tools.id, tools.name, tools.enabled, tools.url, tools.icon, tools.type FROM users_quicklinks ' +
                            'INNER JOIN users on users_quicklinks.user_id = users.id INNER JOIN tools ON users_quicklinks.tool_id = ' +
                            'tools.id WHERE users.id = $1';
                        return t.any(query, [user.id]).then(result => {
                            return res.json({
                                code: 'S0003',
                                status: 'success',
                                data: result,
                                message: 'Records found.',
                                origin: 'User Endpoint - Fetch user quicklinks',
                                priority: 10
                            })
                        }).catch(err => {
                            return res.json({
                                code: 'S0004',
                                status: 'failure',
                                message: 'An error has occurred.',
                                data: err,
                                origin: 'User Endpoint - Fetch user quicklinks',
                                priority: 8
                            })
                        })
                    }
                })
            }).catch(error => {
                return res.json({
                    code: 'S0005',
                    status: 'failure',
                    message: 'An error has occurred.',
                    data: error,
                    origin: 'User Endpoint - Fetch user quicklinks',
                    priority: 8
                })
            });
        } else {
            if (parseInt(req.params.id) === token.id) {
                let query = 'SELECT tools.id, tools.name, tools.enabled, tools.url, tools.icon, tools.type FROM users_quicklinks ' +
                    'INNER JOIN users on users_quicklinks.user_id = users.id INNER JOIN tools ON users_quicklinks.tool_id = ' +
                    'tools.id WHERE users.id = $1';
                db.any(query, [req.params.id]).then(result => {
                    return res.json({
                        code: 'S0006',
                        status: 'success',
                        data: result,
                        message: 'Records found.',
                        origin: 'User Endpoint - Fetch user quicklinks',
                        priority: 10
                    })
                }).catch(err => {
                    return res.json({
                        code: 'S0007',
                        status: 'failure',
                        message: 'An error has occurred.',
                        data: err,
                        origin: 'User Endpoint - Fetch user quicklinks',
                        priority: 8
                    })
                })
            } else {
                return res.json({
                    code: 'S0008',
                    status: 'failure',
                    message: 'Access denied.',
                    origin: 'User Endpoint - Fetch user quicklinks',
                    priority: 9
                })
            }
        }
    }
});

//Assign leave to a user
router.post('/leave/assign', (req, res, next) => {
    "use strict";
    const origin = 'User Endpoint - Assign leave';
    const token = ft_token.verifyToken(req);
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'We could not verify your token. Please try again.',
            origin: origin,
            priority: 9
        })
    } else if (token.role !== 'administrator' || !token.id) {
        return res.json({
            code: 'S0002',
            status: 'failure',
            message: 'Access denied.',
            origin: origin,
            priority: 9
        })
    } else if (!req.body.startDate) {
        return res.json({
            code: 'S0003',
            status: 'failure',
            message: 'A start Date is required. Please try again.',
            origin: origin,
            priority: 9
        })
    } else if (!req.body.endDate) {
        return res.json({
            code: 'S0004',
            status: 'failure',
            message: 'An end date is required. Please try again.',
            origin: origin,
            priority: 9
        })
    } else if (new Date(req.body.endDate).getTime() < new Date(req.body.startDate).getTime()) {
        return res.json({
            code: 'S0004',
            status: 'failure',
            message: 'The provided leave end date cannot be before the start date. Please try again.',
            origin: origin,
            priority: 9
        })
    } else if (!req.body.reason) {
        return res.json({
            code: 'S0005',
            status: 'failure',
            message: 'A reason for leave is required. Please try again.',
            origin: origin,
            priority: 9
        })
    } else if (!req.body.users || !req.body.users.length) {
        return res.json({
            code: 'S0005',
            status: 'failure',
            message: 'Leave must be assigned to valid users. Please try again.',
            origin: origin,
            priority: 9
        })
    } else {
        db.tx(t => {
            const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
            const queries = [];
            return t.one('SELECT id, username, first_name, last_name FROM users WHERE id = $1 LIMIT 1', [token.id])
                .then(notifier => {
                    if (req.body.users) {
                        for (const user of req.body.users) {
                            const message = 'has assigned leave to you from ' +
                                new Date(req.body.startDate).getDate() + ' ' + months[new Date(req.body.startDate).getMonth()] + ' ' +
                                new Date(req.body.startDate).getFullYear() + ' to ' + new Date(req.body.endDate).getDate() + ' ' +
                                months[new Date(req.body.endDate).getMonth()] + ' ' + new Date(req.body.endDate).getFullYear();
                            queries.push(t.none('INSERT INTO users_leave(user_id, start_date, end_date, reason, approved)' +
                                'VALUES($1, $2, $3, $4, true) ON CONFLICT ON CONSTRAINT uc_users_leave DO NOTHING', [user.id, new Date(req.body.startDate), new Date(req.body.endDate), req.body.reason]));
                            queries.push(t.none('INSERT INTO users_notifications(notifier_id, notifyee_id, message, type) ' +
                                'VALUES ($1, $2, $3, \'action\') ON CONFLICT ON CONSTRAINT uc_users_notifications DO ' +
                                'UPDATE SET date_sent = now()', [token.id, user.id, message]).then(res => {
                                    req.app.locals.io.emit('user-notification', {
                                        notifier: notifier,
                                        notifyee: user,
                                        message: message,
                                        type: 'action'
                                    });
                                })
                            );
                        }
                    }
                    return t.batch(queries).catch(error => { throw(error) });
                }).catch(error => {
                    console.log(error);
                    throw error
                });
        }).then(result => {
            return res.json({
                code: 'S0006',
                status: 'success',
                message: 'Leave assigned successfully',
                data: req.body,
                origin: origin,
                priority: 10
            })
        }).catch(error => {
            return res.json({
                code: 'S0007',
                status: 'failure',
                message: 'We could not assign the provided leave. Please try again.',
                data: error,
                origin: origin,
                priority: 8
            })
        })
    }
});

//Assign new quicklink to user
router.post('/:id/quicklinks', (req, res, next ) => {
    "use strict";
    const token = ft_token.verifyToken(req);
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'We could not verify your token.',
            origin: 'User Endpoint - New user quicklink',
            priority: 9
        })
    } else {
        if (!req.body.toolId) {
            return res.json({
                code: 'S0002',
                status: 'failure',
                message: 'A \'toolId\' variable is required in the body of your request.',
                origin: 'User Endpoint - New user quicklink',
                priority: 9
            })
        } else if (parseInt(req.params.id) !== token.id) {
            return res.json({
                code: 'S0003',
                status: 'failure',
                message: 'Access Denied.',
                origin: 'User Endpoint - New user quicklink',
                priority: 9
            })
        } else {
            let query = '';
            if (parseInt(req.params.id)) {
                query = '$1';
            } else {
                query = '(SELECT id FROM users WHERE username = $1 LIMIT 1)'
            }
            db.none('INSERT INTO users_quicklinks(user_id, tool_id) VALUES(' + query + ', $2) ON CONFLICT ON CONSTRAINT uc_users_quicklinks DO NOTHING' , [req.params.id, parseInt(req.body.toolId)]).then(result => {
                return res.json({
                    code: 'S0004',
                    status: 'success',
                    message: 'Quicklink added successfully',
                    origin: 'User Endpoint - New user quicklink',
                    priority: 10
                })
            }).catch(err => {
                return res.json({
                    code: 'S0002',
                    status: 'failure',
                    message: 'An error has occurred.',
                    data: err,
                    origin: 'User Endpoint - Fetch user quicklinks',
                    priority: 8
                })
            })

        }
    }
});

//Remove a quicklink from user
router.delete('/:id/quicklinks', (req, res, next ) => {
    "use strict";
    const token = ft_token.verifyToken(req);
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'We could not verify your token.',
            origin: 'User Endpoint - Delete user quicklink',
            priority: 9
        })
    } else {
        if (!req.query.tool_id) {
            return res.json({
                code: 'S0002',
                status: 'failure',
                message: 'A \'tool_id\' query parameter is required in the url of your request.',
                origin: 'User Endpoint - Delete user quicklink',
                priority: 9
            })
        } else if (parseInt(req.params.id) !== token.id) {
            return res.json({
                code: 'S0003',
                status: 'failure',
                message: 'Access Denied.',
                origin: 'User Endpoint - Delete user quicklink',
                priority: 9
            })
        } else {
            let query = '';
            if (parseInt(req.params.id)) {
                query = '$1';
            } else {
                query = '(SELECT id FROM users WHERE username = $1 LIMIT 1)'
            }
            db.any('DELETE FROM users_quicklinks WHERE user_id = ' + query + ' AND tool_id = $2' , [req.params.id, parseInt(req.query.tool_id)]).then(result => {
                if (result) {
                    return res.json({
                        code: 'S0004',
                        status: 'success',
                        message: 'Quicklink removed successfully',
                        origin: 'User Endpoint - Delete user quicklink',
                        priority: 10
                    })
                }
            }).catch(err => {
                console.log(err);
                return res.json({
                    code: 'S0002',
                    status: 'failure',
                    message: 'An error has occurred.',
                    data: err,
                    origin: 'User Endpoint - Delete user quicklink',
                    priority: 8
                })
            })

        }
    }
});

//Get all notes for a user
router.get('/:id/notes', (req, res, next) => {
    "use strict";
    const token = ft_token.verifyToken(req);
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'We could not verify your token.',
            origin: 'User Endpoint - Get user notes',
            priority: 9
        })
    } else {
        let query = '';
        let queryArray = [];
        switch(token.role) {
            case 'administrator':
                query += 'SELECT users.first_name AS note_taking_user_first_name, users.last_name AS ' +
                    'note_taking_user_last_name, users.username AS note_taking_user_username, ' +
                    'users.profile_image AS note_taking_user_profile_image, users.id AS ' +
                    'note_taking_user_id, users_notes.priority, users_notes.id, users_notes.title, users_notes.content, ' +
                    'users_notes.date FROM users_notes INNER JOIN users ON ' +
                    'users_notes.note_taking_user = users.id WHERE users_notes.user_id = ';
                break;
            case 'staff':
                query += 'SELECT users.first_name AS note_taking_user_first_name, users.last_name AS ' +
                    'note_taking_user_last_name, users.username AS note_taking_user_username, ' +
                    'users.profile_image AS note_taking_user_profile_image, users.id AS ' +
                    'note_taking_user_id, users_notes.priority, users_notes.id, users_notes.title, users_notes.content, ' +
                    'users_notes.date FROM users_notes INNER JOIN users ON ' +
                    'users_notes.note_taking_user = users.id WHERE users.role = \'staff\' AND ' +
                    'users_notes.user_id = ';
                break;
            default:
                query += 'SELECT users.first_name AS note_taking_user_first_name, users.last_name AS ' +
                'note_taking_user_last_name, users.username AS note_taking_user_username, ' +
                'users.profile_image AS note_taking_user_profile_image, users.id AS ' +
                'note_taking_user_id, users_notes.priority, users_notes.id, users_notes.title, users_notes.content, ' +
                'users_notes.date FROM users_notes INNER JOIN users ON ' +
                'users_notes.note_taking_user = users.id WHERE notes_users.note_taking_user = ';
                break;
        }
        if (parseInt(req.params.id)) {
            query += '$1';
            queryArray.push(parseInt(req.params.id));
        } else {
            query += '(SELECT id FROM users WHERE username = $1 LIMIT 1)';
            queryArray.push(req.params.id);
        }
        query += ' ORDER BY users_notes.date DESC';
        db.any(query, queryArray).then(records => {
            return res.json({
                code: 'S0004',
                status: 'success',
                data: records,
                message: 'Records found.',
                origin: 'User Endpoint - Get user notes',
                priority: 10
            })
        }).catch(err => {
            console.log(err);
            return res.json({
                code: 'S0005',
                status: 'failure',
                message: 'An error has occurred.',
                data: err,
                origin: 'User Endpoint - Get user notes',
                priority: 8
            })
        });
    }
});

//Create a new note for a user
router.post('/notes', (req, res, next) => {
    "use strict";
    const token = ft_token.verifyToken(req);
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'We could not verify your token.',
            origin: 'User Endpoint - New user note',
            priority: 9
        })
    } else {
        if (!req.body.note_taking_user) {
            return res.json({
                code: 'S0002',
                status: 'failure',
                message: 'A \'note_taking_user\' variable is required in the body of your request.',
                origin: 'User Endpoint - New user note',
                priority: 9
            })
        } else if (!req.body.user_id) {
            return res.json({
                code: 'S0003',
                status: 'failure',
                message: 'A \'user_id\' variable is required in the body of your request.',
                origin: 'User Endpoint - New user note',
                priority: 9
            })
        } else if (!req.body.title) {
            return res.json({
                code: 'S0004',
                status: 'failure',
                message: 'A \'title\' variable is required in the body of your request.',
                origin: 'User Endpoint - New user note',
                priority: 9
            })
        } else if (!req.body.content) {
            return res.json({
                code: 'S0005',
                status: 'failure',
                message: 'A \'content\' variable is required in the body of your request.',
                origin: 'User Endpoint - New user note',
                priority: 9
            })
        } else if (!req.body.priority) {
            return res.json({
                code: 'S0006',
                status: 'failure',
                message: 'A \'priority\' variable is required in the body of your request.',
                origin: 'User Endpoint - New user note',
                priority: 9
            })
        } else {
            let queryArray = [];
            let query = 'INSERT INTO users_notes(user_id, note_taking_user, title, content, priority, date) ';
            if (parseInt(req.body.user_id)) {
                query += 'VALUES($1,';
                queryArray.push(parseInt(req.body.user_id));
            } else {
                query += 'VALUES((SELECT id FROM users WHERE username = $1 LIMIT 1),';
                queryArray.push(req.params.id);
            }
            if (parseInt(req.body.note_taking_user)) {
                query += '$2,';
                queryArray.push(parseInt(req.body.note_taking_user));
            } else {
                query += '(SELECT id FROM users WHERE username = $2 LIMIT 1),';
                queryArray.push(req.params.id);
            }
            query += '$3, $4, $5, NOW()) ON CONFLICT ON CONSTRAINT uc_users_notes DO NOTHING';
            queryArray.push(req.body.title);
            queryArray.push(req.body.content);
            queryArray.push(req.body.priority);
            db.none(query, queryArray).then(records => {
                return res.json({
                    code: 'S0007',
                    status: 'Success',
                    message: 'Note added successfully.',
                    origin: 'User Endpoint - New user note',
                    priority: 10
                });
            }).catch(err => {
                console.log(err);
                return res.json({
                    code: 'S0008',
                    status: 'failure',
                    message: 'An error has occurred.',
                    data: err,
                    origin: 'User Endpoint - New user note',
                    priority: 8
                })
            });
        }
    }
});

//Update a bootcamp for a user
router.patch('/:id/bootcamps/:bootcampId', (req, res, next) => {
    "use strict";
    const origin = 'Users Endpoint - Update user bootcamp information';
    const token = ft_token.verifyToken(req);
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'We could not verify your token.',
            origin: origin,
            priority: 9
        })
    } else if (token.role !== 'administrator' && token.role !== 'staff') {
        return res.json({
            code: 'S0002',
            status: 'failure',
            message: 'Access denied.',
            origin: origin,
            priority: 9
        })
    } else if (!token.id) {
        return res.json({
            code: 'S0003',
            status: 'failure',
            message: 'a token id is required.',
            origin: origin,
            priority: 9
        })
    } else if (!req.params.bootcampId) {
        return res.json({
            code: 'S0003',
            status: 'failure',
            message: 'a token bootcamp id is required.',
            origin: origin,
            priority: 9
        })
    } else {
        db.task(t => {
            let query = 'user_id = $1';
            if (isNaN(req.params.id)) {
                query = 'user_id = (SELECT id from users WHERE username = $1)';
            }
            return t.any('SELECT * FROM users_bootcampers WHERE ' + query, req.params.id).then(user => {
                if (!user) {
                    throw res.json({
                        code: 'S0003',
                        status: 'failure',
                        message: 'That user does not exist.',
                        origin: origin,
                        priority: 9
                    })
                } else {
                    let queryPosition = 1;
                    const queryArray = [];
                    query = 'UPDATE users_bootcamps SET ';
                    if (req.body.isAwol !== "undefined" && (req.body.isAwol === true || req.body.isAwol === false)) {
                        queryArray.push(req.body.isAwol);
                        query += 'is_awol = $1';
                        queryPosition++;
                    }
                    let userQuery = 'user_id = $' + queryPosition;
                    if (isNaN(req.params.id)) {
                        userQuery = 'user_id = (SELECT id FROM users WHERE username = $' + queryPosition + ')';
                    }
                    queryArray.push(req.params.id);
                    queryPosition++;
                    let bootcampQuery = 'bootcamp_id = $' + queryPosition;
                    if (isNaN(req.params.bootcampId)) {
                        bootcampQuery = 'bootcamp_id = (SELECT id FROM bootcamps WHERE slug = $' + queryPosition + ')';
                    }
                    queryArray.push(req.params.bootcampId);
                    query += ' WHERE ' + userQuery + ' AND ' + bootcampQuery + ' RETURNING id';
                    if (queryArray.length) {
                        return t.oneOrNone(query, queryArray).then(result => {
                            if (result) {
                                return res.json({
                                    code: 'S0001',
                                    status: 'success',
                                    message: 'Updated user successfully',
                                    origin: origin,
                                    priority: 10
                                });
                            } else {
                                return res.json({
                                    code: 'S0001',
                                    status: 'failure',
                                    message: 'Could not update user',
                                    origin: origin,
                                    priority: 9
                                });
                            }
                        }).catch(error => {
                            console.log(error);
                            return res.json({
                                code: 'S0001',
                                status: 'failure',
                                message: 'Could not update user',
                                origin: origin,
                                priority: 9
                            });
                        })
                    } else {
                        return res.json({
                            code: 'S0001',
                            status: 'failure',
                            message: 'No valid values to update could be found.',
                            origin: origin,
                            priority: 9
                        });
                    }
                }
            }).catch(error => {
                throw res.json({
                    code: 'S0002',
                    status: 'failure',
                    message: 'An error has occurred.',
                    data: 'Could not select user from database.',
                    error: error,
                    origin: origin,
                    priority: 8
                });
            });
        }).catch(error => {
            return res.json({
                code: 'S0002',
                status: 'failure',
                message: 'An error has occurred.',
                data: error,
                origin: origin,
                priority: 8
            });
        });
    }
});

module.exports = router;
