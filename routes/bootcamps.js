const express = require('express');
const router = express.Router();

const db = require('../db').db;
const ft_token = require('../lib/ft_token');
const ft_string = require('../lib/ft_string');
const Bootcamp = require('../models/bootcamp');

// Fetch All campuses
router.get('/', (req, res, next) => {
    const origin = 'Bootcamps endpoint - Fetch all Bootcamps';
    const token = ft_token.verifyToken(req);
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'We could not verify your token.',
            origin: origin,
            priority: 9
        })
    } else if (token.role !== 'administrator' && token.role !== 'staff') {
        return res.json({
            code: 'S0002',
            status: 'failure',
            message: 'Access denied.',
            origin: origin,
            priority: 9
        })
    } else {
        let orderBy = 'title';
        let page = 1;
        let limit = 20;
        if (req.query.page) {
            page = parseInt(req.query.page);
        }
        if (req.query.results_per_page) {
            limit = parseInt(req.query.results_per_page);
        }
        if (req.query.order_by) {
            switch (req.query.order_by) {
                case 'slug':
                    orderBy = req.query.order_by;
                    break;
                default:
                    break;
            }
        }
        let bootcamps = [];
        db.task(t =>  {
            return t.any('SELECT * FROM bootcamps ORDER BY ' + orderBy + ' LIMIT ' + limit + 'OFFSET ' + limit * (page - 1));
        }).then(result => {
            bootcamps = result;
            return db.tx(t => {
                const queries = [];
                for (const bootcamp of bootcamps) {
                    queries.push(t.one('SELECT COUNT(DISTINCT id) FROM users_bootcamps WHERE final_decision = \'accepted\' ' +
                        'AND bootcamp_id = $1 AND role = \'bootcamper\'', [bootcamp.id]).then( resultAccepted => {
                            if (parseInt(resultAccepted.count)) {
                                bootcamp.count_accepted = parseInt(resultAccepted.count);
                            } else {
                                bootcamp.count_accepted = 0;
                            }
                    }));
                    queries.push(t.one('SELECT COUNT(DISTINCT id) FROM users_bootcamps WHERE final_decision = \'declined\' ' +
                        'AND bootcamp_id = $1 AND role = \'bootcamper\'', [bootcamp.id]).then( resultDeclined => {
                        if (parseInt(resultDeclined.count)) {
                            bootcamp.count_declined = parseInt(resultDeclined.count);
                        } else {
                            bootcamp.count_declined = 0;
                        }
                    }));
                    queries.push(t.one('SELECT COUNT(DISTINCT id) FROM users_bootcamps WHERE final_decision = \'retry\' ' +
                        'AND bootcamp_id = $1 AND role = \'bootcamper\'', [bootcamp.id]).then( resultRetry => {
                        if (parseInt(resultRetry.count)) {
                            bootcamp.count_retry = parseInt(resultRetry.count);
                        } else {
                            bootcamp.count_retry = 0;
                        }
                    }));
                    queries.push(t.one('SELECT COUNT(DISTINCT id) FROM users_bootcamps WHERE final_decision IS NULL ' +
                        'AND bootcamp_id = $1 AND role = \'bootcamper\'', [bootcamp.id]).then( resultUndecided=> {
                        if (parseInt(resultUndecided.count)) {
                            bootcamp.count_undecided = parseInt(resultUndecided.count);
                        } else {
                            bootcamp.count_undecided = 0;
                        }
                    }));
                    queries.push(t.one('SELECT COUNT(users_bootcamps.id) FROM users_bootcamps INNER JOIN users_information ON ' +
                        'users_bootcamps.user_id = users_information.user_id WHERE users_information.gender = \'male\' ' +
                        'AND users_bootcamps.bootcamp_id = $1 AND users_bootcamps.role = \'bootcamper\'', [bootcamp.id]).then( resultMale=> {
                        if (parseInt(resultMale.count)) {
                            bootcamp.count_male = parseInt(resultMale.count);
                        } else {
                            bootcamp.count_male = 0;
                        }
                    }));
                    queries.push(t.one('SELECT COUNT(users_bootcamps.id) FROM users_bootcamps INNER JOIN users_information ON ' +
                        'users_bootcamps.user_id = users_information.user_id WHERE users_information.gender = \'female\' ' +
                        'AND bootcamp_id = $1 AND role = \'bootcamper\'', [bootcamp.id]).then(resultFemale=> {
                        if (parseInt(resultFemale.count)) {
                            bootcamp.count_female = parseInt(resultFemale.count);
                        } else {
                            bootcamp.count_female = 0;
                        }
                    }));
                    queries.push(t.one('SELECT COUNT(id) FROM users_bootcamps WHERE bootcamp_id = $1 AND ' +
                        'role = \'bootcamper\'', [bootcamp.id]).then(resultBootcampers => {
                        if (parseInt(resultBootcampers.count)) {
                            bootcamp.count_bootcampers = parseInt(resultBootcampers.count);
                        } else {
                            bootcamp.count_bootcampers = 0;
                        }
                    }));
                }
                return t.batch (queries);
            }).catch(error => {
                throw error;
            })
        }).then(resultBootcamps => {
            return res.json({
                code: 'S0003',
                status: 'success',
                message: 'Results Retrieved Successfully',
                data: bootcamps,
                origin: origin,
                priority: 10
            })
        }).catch(error => {
            console.log(error);
            return res.json({
                code: 'S0004',
                status: 'failure',
                message: 'An error has occurred.',
                data: error,
                origin: origin,
                priority: 8
            })
        });
    }
});

router.get('/:id', (req, res, next) => {
    const origin =  'Bootcamps endpoint - Fetch single bootcamp';
    const token = ft_token.verifyToken(req);
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'We could not verify your token.',
            origin: origin,
            priority: 9
        })
    } else if (!req.params.id) {
        return res.json({
            code: 'S0002',
            status: 'failure',
            message: 'A bootcamp id is required. Please try again.',
            origin: origin,
            priority: 9
        })
    } else {
        let query = 'id = $1';
        if (isNaN(req.params.id)) {
            query = 'slug = $1';
        }
        db.task( t => {
            return t.oneOrNone('SELECT id, title, slug, start_date, end_date, selection_date, current_step FROM bootcamps WHERE ' + query, [req.params.id]);
        }).then(result => {
            if (result) {
                    return res.json({
                        code: 'S0003',
                        status: 'success',
                        message: 'results retrieved successfully.',
                        data: result,
                        origin: origin,
                        priority: 10
                    })
                } else {
                return res.json({
                    code: 'S0004',
                    status: 'failure',
                    message: 'That bootcamp does not exist.',
                    origin: origin,
                    priority: 9
                });
            };
        }).catch(error => {
            if (error) {
                return res.json({
                    code: 'S0005',
                    status: 'failure',
                    message: 'An error has occurred.',
                    data: error,
                    origin: origin,
                    priority: 8
                })
            }
        });
    }
});

router.patch('/:id/finalize', (req, res, next) => {
    "use strict";
    const origin = 'Bootcamps Endpoint - Finalize a bootcamp';
    const token = ft_token.verifyToken(req);
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'We could not verify your token.',
            origin: origin,
            priority: 9
        })
    } else if (token.role !== 'administrator' && token.role !== 'staff') {
        return res.json({
            code: 'S0002',
            status: 'failure',
            message: 'Access denied.',
            origin: origin,
            priority: 9
        })
    } else if (!token.id) {
        return res.json({
            code: 'S0003',
            status: 'failure',
            message: 'a token id is required.',
            origin: origin,
            priority: 9
        })
    } else {
        db.tx(t => {
            let fetchedBootcamps;
            t.oneOrNone('SELECT * from bootcamps WHERE id = $1 LIMIT 1', req.params.id)
                .then(bootcamps => {
                    if (bootcamps) {
                        fetchedBootcamps = bootcamps;
                    } else {
                        throw 'That bootcamp does not exist.';
                    }
                }).then(emptyRes => {
                    return db.tx(transaction => {
                        return transaction.none('UPDATE bootcamps SET finalized = true WHERE id = $1', req.params.id).then(updated => {
                            req.app.locals.io.emit('finalize-bootcamp', { user: token, bootcamp: fetchedBootcamps, api_key: process.env.AUTOBOT_API_KEY });
                            return res.json({
                                code: 'S0004',
                                status: 'success',
                                message: 'Bootcamp finalization process started.',
                                origin: origin,
                                priority: 10
                            })
                        });
                    })
                }).catch(error => {
                console.log(error);
                    return res.json({
                        code: 'S0005',
                        status: 'failure',
                        message: error,
                        origin: origin,
                        priority: 9
                    })
                });
        });
    }
});

router.post('/:id/users', (req, res, next) => {
    const origin = 'Bootcamps Endpoint - New bootcamp';
    const token = ft_token.verifyToken(req);
    const response = { new: [], updated: [], rejected:[] };
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'We could not verify your token.',
            origin: origin,
            priority: 9
        })
    } else if (token.role !== 'administrator') {
        return res.json({
            code: 'S0002',
            status: 'failure',
            message: 'Access Denied.',
            origin: origin,
            priority: 9
        })
    } else if (!req.body.users || !req.body.users.length) {
        return res.json({
            code: 'S0003',
            status: 'failure',
            message: 'Users are required.',
            origin: origin,
            priority: 9
        })
    } else {
        let query = 'id = $1';
        if (isNaN(req.params.id)) {
            query = 'slug = $1'
        }
        let role = 'bootcamper';
        if (req.query.role) {
            switch(req.query.role) {
                case 'staff':
                    role = 'staff';
                    break;
                default:
                    break;
            }
        }
        return db.tx(t => {
            return t.oneOrNone('SELECT * FROM bootcamps WHERE ' + query, [req.params.id]).then(fetchingBootcampResult => {
                if (!fetchingBootcampResult) {
                    throw res.json({
                        code: 'S0004',
                        status: 'failure',
                        message: 'That bootcamp does not exist.',
                        origin: origin,
                        priority: 9
                    })
                }
                let queries = [];
                for (const user of req.body.users) {
                    if (user.username && user.email && user.firstName && user.lastName) {
                        queries.push(
                            t.oneOrNone('SELECT id from users WHERE username = $1', [user.username]).then(result => {
                                if (!result) {
                                    response.rejected.push({user: user, reason: 'User does not exist.'})
                                } else {
                                    user.id = result.id;
                                    response.updated.push({user: user, reason: 'User already exists.'})
                                }
                            })
                        );
                        queries.push(
                            t.none('INSERT INTO users_bootcamps(user_id, bootcamp_id, role) VALUES ((SELECT id ' +
                                'FROM users WHERE username = $1), $2, $3) ON CONFLICT ON CONSTRAINT uc_users_bootcamps ' +
                                'DO UPDATE SET role = $3', [user.username, req.params.id, role])
                        );
                        let date = new Date(fetchingBootcampResult.start_date);
                        const months = ['january', 'february', 'march', 'april', 'may', 'june', 'july',
                            'august', 'september', 'october', 'november', 'december'];
                        queries.push(t.none('INSERT INTO users_bootcampers(user_id, level, bootcamp_month,' +
                            'bootcamp_year) VALUES ((SELECT id FROM users WHERE username = $1), 0, $2, $3) ON CONFLICT ON CONSTRAINT uc_users_bootcampers_secondary DO UPDATE SET bootcamp_month = $2, bootcamp_year = $3',[user.username, months[date.getMonth()], date.getFullYear()]))
                    } else {
                        response.rejected.push({user: user, reason: 'Incorrect parameters supplied.'})
                    }
                }
                return t.batch(queries);
            });
        }).then(addingUsersResult => {
            return res.json({
                code: 'S0005',
                status: 'success',
                message: 'Bootcamp users added Successfully',
                data: response,
                origin: origin,
                priority: 10
            })
        }).catch(error => {
            console.log(error);
            return res.json({
                code: 'S0006',
                status: 'failure',
                message: 'An error has occurred.',
                data: error,
                origin: origin,
                priority: 8
            })
        });

    }
});

router.post('/:id/votes', (req, res, next) => {
    const origin = 'Bootcamps Endpoint - New bootcamp';
    const token = ft_token.verifyToken(req);
    const response = { new: [], updated: [], rejected:[] };
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'We could not verify your token.',
            origin: origin,
            priority: 9
        })
    } else if (token.role !== 'administrator') {
        return res.json({
            code: 'S0002',
            status: 'failure',
            message: 'Access Denied.',
            origin: origin,
            priority: 9
        })
    } else if (!req.body.votes || !req.body.votes.length) {
        return res.json({
            code: 'S0003',
            status: 'failure',
            message: 'votes are required.',
            origin: origin,
            priority: 9
        })
    } else {
        let query = 'id = $1';
        if (isNaN(req.params.id)) {
            query = 'slug = $1'
        }
        let role = 'bootcamper';
        if (req.query.role) {
            switch(req.query.role) {
                case 'staff':
                    role = 'staff';
                    break;
                default:
                    break;
            }
        }
        return db.tx(t => {
            return t.oneOrNone('SELECT * FROM bootcamps WHERE ' + query, [req.params.id]).then(fetchingBootcampResult => {
                if (!fetchingBootcampResult) {
                    throw res.json({
                        code: 'S0004',
                        status: 'failure',
                        message: 'That bootcamp does not exist.',
                        origin: origin,
                        priority: 9
                    })
                }
                let queries = [];
                for (const vote of req.body.votes) {
                    if (vote.voter && vote.votee) {
                        let votersCount = 0;
                        queries.push(
                            t.any('SELECT id from users WHERE username = $1 OR username = $2', [vote.voter, vote.votee]).then(result => {
                                votersCount = result.length;
                                if (votersCount !== 2 && vote.voter !== 'system') {
                                    response.rejected.push({vote: vote, reason: 'Users do not exist.'})
                                } else {
                                    response.updated.push({vote: vote, reason: 'Users already exist.'})
                                }
                            })
                        );
                        if (votersCount === 2 || (votersCount !== 1 && vote.voter === 'system')) {
                            queries.push(
                                t.oneOrNone('INSERT INTO users_votes(voter_id, votee_id, bootcamp_id, role, type, date) VALUES ((SELECT id FROM users WHERE username = $1), (SELECT id FROM users WHERE username = $2), $3, $4, \'bootcamp\', now())', [vote.voter, vote.votee, req.params.id, role])
                            )
                        }
                    } else {
                        response.rejected.push({vote: vote, reason: 'Incorrect parameters supplied.'});
                    }
                }
                return t.batch(queries);
            });
        }).then(addingUsersResult => {
            return res.json({
                code: 'S0005',
                status: 'success',
                message: 'Bootcamp user votes added Successfully',
                data: response,
                origin: origin,
                priority: 10
            })
        }).catch(error => {
            console.log(error);
            return res.json({
                code: 'S0006',
                status: 'failure',
                message: 'An error has occurred.',
                data: error,
                origin: origin,
                priority: 8
            })
        });

    }
});

router.delete('/:id/users', (req, res, next) => {
    "use strict";
    const origin = 'Bootcamps Endpoint - Delete user';
    const token = ft_token.verifyToken(req);
    const response = { new: [], updated: [], rejected:[] };
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'We could not verify your token.',
            origin: origin,
            priority: 9
        })
    } else if (token.role !== 'administrator') {
        return res.json({
            code: 'S0002',
            status: 'failure',
            message: 'Access Denied.',
            origin: origin,
            priority: 9
        })
    } else if (!req.query.user_id) {
        return res.json({
            code: 'S0003',
            status: 'failure',
            message: 'A user ID is required. Please try again.',
            origin: origin,
            priority: 9
        })
    } else {
        let queryUser = 'user_id = $1';
        let queryBootcamp = 'bootcamp_id = $2';
        if (isNaN(req.query.user_id)) {
            queryUser = 'user_id = (SELECT id FROM users WHERE username = $1)';
        }
        if(isNaN(req.params.id)) {
            queryBootcamp = 'bootcamp_id = (SELECT id FROM bootcamps WHERE slug = $2)';
        }
        let role = 'bootcamper';
        if (req.query.role) {
            switch(req.query.role) {
                case 'staff':
                    role = 'staff';
                    break;
                default: break;
            }
        }
        return db.task(t => {
            return t.oneOrNone('SELECT id FROM users_bootcamps WHERE ' + queryUser + ' AND ' + queryBootcamp + ' AND role = $3', [req.query.user_id, req.params.id, role])
                .then(userBootcamp => {
                    if (userBootcamp) {
                        return t.none('DELETE FROM users_bootcamps WHERE id = $1', [userBootcamp.id])
                    } else {
                        throw res.json({
                            code: 'S0004',
                            status: 'failure',
                            message: 'That record does not exist. Please try again.',
                            origin: origin,
                            priority: 9
                        })
                    }
                })
        }).then(result => {
            return res.json({
                code: 'S0005',
                status: 'success',
                message: 'Bootcamp user deleted successfully',
                origin: origin,
                priority: 10
            })
        }).catch(error => {
            //console.log(error);
            return res.json({
                code: 'S0006',
                status: 'failure',
                message: 'An error has occurred.',
                data: error,
                origin: origin,
                priority: 8
            })
        })
    }
});

router.get('/:id/users', (req, res, next) => {
    const origin =  'Bootcamps endpoint - Fetch all users';
    const token = ft_token.verifyToken(req);
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'We could not verify your token.',
            origin: origin,
            priority: 9
        })
    } else if (!req.params.id) {
        return res.json({
            code: 'S0002',
            status: 'failure',
            message: 'A bootcamp id is required. Please try again.',
            origin: origin,
            priority: 9
        })
    } else {
        let orderBy = 'username';
        let page = 1;
        let limit = 20;
        if (req.query.page) {
            page = parseInt(req.query.page);
        }
        if (req.query.results_per_page) {
            limit = parseInt(req.query.results_per_page);
        }
        if (req.query.order_by) {
            switch (req.query.order_by) {
                case 'username':
                    orderBy = 'users.username';
                    break;
                default:
                    break;
            }
        }
        let role = 'bootcamper';
        if (req.query.role) {
            switch(req.query.role) {
                case 'staff':
                    role = 'staff';
                    break;
                default: break;
            }
        }
        let query = 'users_bootcamps.bootcamp_id = $1';
        if (isNaN(req.params.id)) {
            query = 'users_bootcamps.bootcamp_id = (SELECT id FROM bootcamps WHERE slug = $1)';
        }
        let users = [];
        return db.tx(t => {
            return t.any('SELECT DISTINCT (users.id), users.username, users.email, users.first_name, users.last_name, users.role, ' +
                'users_information.gender, users_information.birth_date, users_information.home_town, ' +
                'users_information.country, users_information.ethnicity, users_information.birth_country, ' +
                'users_information.birth_city, users_information.education, users_information.code_experience, ' +
                'users_bootcamps.final_decision, users_bootcamps.is_awol FROM users_bootcamps INNER JOIN ' +
                'users_bootcampers ON users_bootcamps.user_id = users_bootcampers.user_id INNER JOIN users ON ' +
                'users_bootcamps.user_id = users.id INNER JOIN users_information on users_bootcamps.user_id = ' +
                'users_information.user_id WHERE ' + query + ' AND users_bootcamps.role = $2 ' +
                'ORDER BY ' + orderBy + ' LIMIT ' + limit + 'OFFSET ' + limit * (page - 1), [req.params.id, role]).then( usersResult => {
                    users = usersResult;
                    let queries = [];
                    if (role !== 'staff') {
                        for (const user of users) {
                            user.projects = [];
                            user.corrections = [];
                            user.level = 0;
                            user.student_votes = 0;
                            queries.push(
                                t.any('SELECT * FROM users_projects INNER JOIN projects ON users_projects.project_id = ' +
                                    'projects.id WHERE users_projects.user_id = $1', user.id).then(projects => {
                                    "use strict";
                                    user.projects = projects;
                                })
                            );
                            query = 'bootcamp_id = $2';
                            if (isNaN(req.params.id)) {
                                query = '(SELECT id from bootcamps WHERE slug = $2)'
                            }
                            queries.push(
                                t.any('SELECT * from users_votes WHERE votee_id = $1 AND ' + query, [user.id, req.params.id]).then(votes => {
                                        user.student_votes = votes.length;
                                })
                            );
                            query = 'id = $1';
                            if (isNaN(req.params.id)) {
                                query = '(SELECT id FROM bootcamps WHERE slug = $1)';
                            }
                            queries.push(
                                t.any('SELECT COUNT(*) FROM users_corrections WHERE (correcting_user_id = $2 OR corrected_user_id = $2) AND date_created >= (SELECT start_date FROM bootcamps WHERE ' + query + ') AND date_created <= (SELECT end_date FROM bootcamps WHERE ' + query + ')', [req.params.id, user.id]).then(corrections => {
                                    "use strict";
                                    user.corrections_count = corrections[0].count;
                                })
                            );
                            queries.push(
                                t.one('SELECT COUNT(*) FROM (SELECT DISTINCT ON (date(users_attendance.access_date), users.id) users_attendance.access_date ' +
                                    'FROM users_attendance INNER JOIN users ON ' +
                                    'users_attendance.user_id = users.id WHERE access_date >= (SELECT start_date FROM bootcamps WHERE ' + query + ') ' +
                                    ' AND access_date <= (SELECT end_date FROM bootcamps WHERE ' + query + ') AND username = $2) AS temp', [req.params.id, user.username]).then(attendance => {
                                    user.attendance = parseInt(attendance.count);
                                })
                            );
                            queries.push(
                                t.one('SELECT level FROM users_bootcampers WHERE user_id = $1 ORDER BY level DESC LIMIT 1', [user.id]).then(bootcamper => {
                                    user.level = bootcamper.level;
                                }));
                        }
                    }
                    return t.batch(queries);
                }).catch(error => {
                throw res.json({
                    code: 'S0003',
                    status: 'failure',
                    message: 'An error has occurred. Please try again.',
                    data: error,
                    origin: origin,
                    priority: 8
                })
            })
        }).then(bootcampUsers => {
            return res.json({
                code: 'S0004',
                status: 'success',
                message: 'results retrieved successfully.',
                data: users,
                origin: origin,
                priority: 10
            })
        }).catch(error => {
            console.log(error);
            throw res.json({
                code: 'S0005',
                status: 'failure',
                message: 'An error has occurred. Please try again.',
                data: error,
                origin: origin,
                priority: 8
            })
        })

    }
});

router.post('/', (req, res, next) => {
    const origin = 'Bootcamps Endpoint - New bootcamp';
    const token = ft_token.verifyToken(req);
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'We could not verify your token.',
            origin: origin,
            priority: 9
        })
    } else if (token.role !== 'administrator') {
        return res.json({
            code: 'S0002',
            status: 'failure',
            message: 'Access Denied.',
            origin: origin,
            priority: 9
        })
    } else {
        if (!req.body.title) {
            return res.json({
                code: 'S0003',
                status: 'failure',
                message: 'A campus title is required. Please try again.',
                origin: origin,
                priority: 9
            })
        } else if (!req.body.startDate) {
            return res.json({
                code: 'S0004',
                status: 'failure',
                message: 'A bootcamp start date is required. Please try again.',
                origin: origin,
                priority: 9
            })
        } else if (!req.body.endDate) {
            return res.json({
                code: 'S0005',
                status: 'failure',
                message: 'A bootcamp end date is required. Please try again.',
                origin: origin,
                priority: 9
            })
        } else if (!req.body.selectionDate) {
            return res.json({
                code: 'S0006',
                status: 'failure',
                message: 'A bootcamp selection date is required. Please try again.',
                origin: origin,
                priority: 9
            })
        } else {
            let bootcamp = new Bootcamp(req.body);
            let queryArray = [bootcamp.slug, bootcamp.title, bootcamp.startDate, bootcamp.endDate, bootcamp.selectionDate];
            db.task(t => {
                "use strict";
                return t.oneOrNone('SELECT * FROM bootcamps WHERE title = $1 OR slug = $2 LIMIT 1', [bootcamp.title, bootcamp.slug])
                    .then(bootcamp => {
                        if (bootcamp) {
                            return res.json({
                                code: 'S0007',
                                status: 'failure',
                                message: 'That bootcamp already exists. Please try again.',
                                origin: origin,
                                priority: 8
                            })
                        } else {
                            return t.none('INSERT INTO bootcamps(slug, title, start_date, end_date, selection_date) ' +
                                'VALUES($1, $2, $3, $4, $5)', queryArray)
                                .catch(insertError => {
                                    throw res.json({
                                        code: 'S0008',
                                        status: 'failure',
                                        message: 'We couldn\'t add the requested bootcamp. Please try again.',
                                        data: insertError,
                                        origin: origin,
                                        priority: 8
                                    })
                                })
                        }
                    }).catch(error => {
                        throw res.json({
                            code: 'S0009',
                            status: 'failure',
                            message: 'We couldn\'t check if that bootcamp exists. Please try again.',
                            data: error,
                            origin: origin,
                            priority: 8
                        })
                    })
            }).then(result => {
                "use strict";
                return res.json({
                    code: 'S0010',
                    status: 'success',
                    message: 'Bootcamp added Successfully',
                    origin: origin,
                    priority: 10
                })
            }).catch(error => {
                return res.json({
                    code: 'S0011',
                    status: 'failure',
                    message: 'An error has occurred.',
                    data: error,
                    origin: origin,
                    priority: 8
                })
            });
        }
    }
});

module.exports = router;