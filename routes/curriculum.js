const express = require('express');
const router = express.Router();

const db = require('../db').db;
const ft_token = require('../lib/ft_token');
const ft_string = require('../lib/ft_string');
const Curriculum = require('../models/curriculum');

router.get('/', (req, res, next) => {
    const token = ft_token.verifyToken(req);
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'We could not verify your token.',
            origin: 'Curriculum endpoint - Get all curriculum',
            priority: 9
        })
    } else {
        let orderBy = 'title';
        let page = 1;
        let limit = 30;
        if (req.query.page) {
            page = parseInt(req.query.page);
        }
        if (req.query.results_per_page) {
            limit = parseInt(req.query.results_per_page);
        }
        if (req.query.order_by) {
            switch (req.query.order_by) {
                case '':
                    orderBy = req.query.order_by;
                    break;
                default:
                    break;
            }
        }
        db.task('Get Curriculum', function* (t) {
            const curriculum = yield t.any('SELECT * FROM curriculum ORDER BY ' + orderBy + ' LIMIT ' + limit + 'OFFSET ' + limit * (page - 1));
            for (const item of curriculum) {
                item.campuses = [];
                item.campuses = yield t.any('SELECT * FROM curriculum_campuses INNER JOIN campuses ON curriculum_campuses.campus_id = campuses.id WHERE curriculum_campuses.curriculum_id = $1', [item.id]);
            }
            return curriculum;
        }).then(result => {
            return res.json({
                code: 'S0002',
                status: 'success',
                message: 'Results Retrieved Successfully',
                data: result,
                origin: 'Curriculum endpoint - Get all curriculum',
                priority: 10
            })
        }).catch(error => {
            console.log(error);
            return res.json({
                code: 'S0003',
                status: 'failure',
                message: 'An error has occurred.',
                data: error,
                origin: 'Curriculum endpoint - Get all curriculum',
                priority: 8
            })
        });
    }
});

router.post('/', (req, res, next) => {
    const token = ft_token.verifyToken(req);
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'We could not verify your token.',
            origin: 'Curriculum endpoint - New curriculum',
            priority: 9
        })
    } else if (token.role !== 'administrator') {
        return res.json({
            code: 'S0002',
            status: 'failure',
            message: 'Access Denied.',
            origin: 'Curriculum endpoint - New curriculum',
            priority: 9
        })
    } else {
        if (!req.body.title) {
            return res.json({
                code: 'S0003',
                status: 'failure',
                message: 'A campus title is required. Please try again.',
                origin: 'Curriculum endpoint - New curriculum',
                priority: 9
            })
        } else {
            let curriculum = new Curriculum(req.body);
            let queryArray = [curriculum.generateSlug(), curriculum.title];
            db.task('Add curriculum', t => {
                return t.one('INSERT INTO curriculum(slug, title) VALUES($1, $2) RETURNING id',
                    queryArray).then(result => {
                    if (result.id && curriculum.campuses && curriculum.campuses.length) {
                        for (const campus of curriculum.campuses) {
                            t.none('INSERT INTO curriculum_campuses(campus_id, curriculum_id) VALUES($1, $2)', [campus.id, result.id])
                                .catch(resultCampusesCurriculum => {
                                "use strict";
                                throw(resultCampusesCurriculum);
                            });
                        }
                    }
                })
            }).then(result => {
                return res.json({
                    code: 'S0004',
                    status: 'success',
                    message: 'Record added successfully',
                    origin: 'Curriculum endpoint - New curriculum',
                    priority: 19
                })
            }).catch( error => {
                console.log(error);
                if (error) {
                    return res.json({
                        code: 'S0005',
                        status: 'failure',
                        message: 'An error has occurred.',
                        data: error,
                        origin: 'Curriculum endpoint - New curriculum',
                        priority: 8
                    })
                }
            });
        }
    }
});

module.exports = router;