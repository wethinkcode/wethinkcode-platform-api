const express = require('express');
const router = express.Router();

const db = require('../db').db;
const ft_token = require('../lib/ft_token');
const ft_string = require('../lib/ft_string');

// Get student attendance
router.get('/attendance', (req, res, next) => {
    "use strict";
    const token = ft_token.verifyToken(req);
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'We could not verify your token.',
            origin: 'Student Endpoint - Fetch all attendance',
            priority: 9
        })
    } else {
        db.task('Get student attendance', function *(t) {
            return yield t.any('SELECT DISTINCT ON (date(users_attendance.access_date), users.id) users_attendance.access_date, users.username,' +
                'users_attendance.id FROM users_attendance INNER JOIN users ON ' +
                'users_attendance.user_id = users.id WHERE access_date >= date_trunc(\'month\', current_date - interval \'1\' month)\n' +
                '  and  access_date < date_trunc(\'month\', current_date)');
        }).then(result => {
            return res.json({
                code: 'S0003',
                status: 'success',
                message: 'Results found.',
                data: result,
                origin: 'Student Endpoint - Fetch all attendance',
                priority: 10
            })
        }).catch(err => {
            console.log(err);
            return res.json({
                code: 'S0004',
                status: 'failure',
                message: 'An error has occurred.',
                data: err,
                origin: 'Student Endpoint - Fetch all attendance',
                priority: 8
            })
        })
    }
});

// Get average student attendance
router.get('/attendance/average', (req, res, next) => {
    "use strict";
    const token = ft_token.verifyToken(req);
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'We could not verify your token.',
            origin: 'Student Endpoint - Fetch all attendance',
            priority: 9
        })
    } else {
        db.task('Get average student attendance', function *(t) {
            return yield t.any('SELECT DISTINCT ON (date(users_attendance.access_date), users.id) users_attendance.access_date, users.username,' +
                'users_attendance.id FROM users_attendance INNER JOIN users ON ' +
                'users_attendance.user_id = users.id WHERE access_date >= date_trunc(\'month\', current_date - interval \'1\' month)\n' +
                '  and  access_date < date_trunc(\'month\', current_date)');
        }).then(result => {
            return res.json({
                code: 'S0003',
                status: 'success',
                message: 'Results found.',
                data: result,
                origin: 'Student Endpoint - Fetch all attendance',
                priority: 10
            })
        }).catch(err => {
            console.log(err);
            return res.json({
                code: 'S0004',
                status: 'failure',
                message: 'An error has occurred.',
                data: err,
                origin: 'Student Endpoint - Fetch all attendance',
                priority: 8
            })
        })
    }
});

module.exports = router;