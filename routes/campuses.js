const express = require('express');
const router = express.Router();

const db = require('../db').db;
const ft_token = require('../lib/ft_token');
const ft_string = require('../lib/ft_string');
const Campus = require('../models/campus');

// Fetch All campuses
router.get('/', (req, res, next) => {
    const token = ft_token.verifyToken(req);
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'We could not verify your token.',
            origin: 'Campus endpoint - New campus',
            priority: 9
        })
    } else {
        let orderBy = 'title';
        let page = 1;
        let limit = 30;
        if (req.query.page) {
            page = parseInt(req.query.page);
        }
        if (req.query.results_per_page) {
            limit = parseInt(req.query.results_per_page);
        }
        if (req.query.order_by) {
            switch (req.query.order_by) {
                case 'last_name':
                    orderBy = req.query.order_by;
                    break;
                default:
                    break;
            }
        }
        db.task('Get Campuses', function* (t) {
            return yield t.any('SELECT * FROM campuses ORDER BY ' + orderBy + ' LIMIT ' + limit + 'OFFSET ' + limit * (page - 1));
        }).then(result => {
            return res.json({
                code: 'S0002',
                status: 'success',
                message: 'Results Retrieved Successfully',
                data: result,
                origin: 'Campus endpoint - Fetch campuses',
                priority: 10
            })
        }).catch(error => {
            return res.json({
                code: 'S0003',
                status: 'failure',
                message: 'An error has occurred.',
                data: error,
                origin: 'Campus endpoint - Fetch campuses',
                priority: 8
            })
        });
    }
});

router.post('/', (req, res, next) => {
	"use strict";
    const token = ft_token.verifyToken(req);
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'We could not verify your token.',
            origin: 'Campus endpoint - New campus',
            priority: 9
        })
    } else if (token.role !== 'administrator') {
        return res.json({
            code: 'S0002',
            status: 'failure',
            message: 'Access Denied.',
            origin: 'Campus endpoint - New campus',
            priority: 9
        })
	} else {
    	if (!req.body.title) {
            return res.json({
                code: 'S0003',
                status: 'failure',
                message: 'A campus title is required. Please try again.',
                origin: 'Campus endpoint - New campus',
                priority: 9
            })
		} else if (!req.body.description) {
            return res.json({
                code: 'S0002',
                status: 'failure',
                message: 'A campus description is required. Please try again.',
                origin: 'Campus endpoint - New campus',
                priority: 9
            })
		} else {
    		let campus = new Campus(req.body);
    		let queryArray = [campus.generateSlug(), campus.title, campus.description, campus.postalUnit, campus.postalFloor,
				campus.postalBuilding, campus.postalStreet, campus.postalCity, campus.postalProvince,
				campus.postalCountry, campus.postalPostalCode, campus.physicalUnit, campus.physicalFloor,
				campus.physicalBuilding, campus.physicalStreet, campus.physicalCity, campus.physicalProvince,
				campus.physicalCountry, campus.physicalPostalCode];
    		db.none('INSERT INTO campuses(slug, title, description, postal_unit, postal_floor, postal_building, ' +
				'postal_street, postal_city, postal_province, postal_country, postal_postal_code, physical_unit,' +
				'physical_floor, physical_building, physical_street, physical_city, physical_province, ' +
				'physical_country, physical_postal_code) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12,' +
				'$13, $14, $15, $16, $17, $18, $19)', queryArray).then(result => {
                return res.json({
                    code: 'S0003',
                    status: 'success',
                    message: 'Campus added Successfully',
                    origin: 'Campus endpoint - New campus',
                    priority: 8
                })
			}).catch( error => {
				console.log(error);
				if (error) {
                    return res.json({
                        code: 'S0004',
                        status: 'failure',
                        message: 'An error has occurred.',
                        data: error,
                        origin: 'Campus endpoint - New campus',
                        priority: 10
                    })
				}
			})
		}
    }
});

module.exports = router;