const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');

const db = require('../db').db;
const ft_token = require('../lib/ft_token');

// Fetch tools for specific user
router.get('/', (req, res, next) => {
	const origin = 'Tools endpoint - Get all tools';
    const token = ft_token.verifyToken(req);
    if (!token){
        return res.json({
            code: 'S0001',
            status: 'failure',
            message: 'We could not verify your token.',
            origin: origin,
            priority: 9
        })
    } else {
        let orderBy = 'name';
        let page = 1;
        let limit = 20;
        if (req.query.page) {
            page = parseInt(req.query.page);
        }
        if (req.query.results_per_page) {
            limit = parseInt(req.query.results_per_page);
        }
        if (req.query.order_by) {
            switch (req.query.order_by) {
                default:
                    break;
            }
        }
        db.task(t => {
            return t.any('SELECT * FROM tools ORDER BY ' + orderBy + ' LIMIT ' + limit + 'OFFSET ' + limit * (page - 1));
        }).then(result => {
            return res.json({
                code: 'S0002',
                status: 'success',
                message: 'Results Retrieved Successfully',
                data: result,
                origin: origin,
                priority: 10
            })
        }).catch(error => {
            console.log(error);
            return res.json({
                code: 'S0003',
                status: 'failure',
                message: 'An error has occurred.',
                data: error,
                origin: origin,
                priority: 8
            })
        });
    }
});

router.get('/:id', (req, res, next) => {
	"use strict";
	const token = ft_token.verifyToken(req);
	if (!token){
		return res.json({
			code: 'S0001',
			status: 'failure',
			message: 'We could not verify your token.',
			origin: 'Tools Endpoint - Fetch user tools',
			priority: 9
		})
	} else {
		db.any('SELECT tools.id, tools.name, tools.enabled, tools.url, tools.icon, tools.type FROM tools INNER JOIN users_tools ON users_tools.tool_id = tools.id AND users_tools.user_id = $1', [req.params.id])
			.then((data) =>{
				if (!data.length) {
					return res.json({
						code: 'S0001',
						status: 'failure',
						message: 'No records found.',
						origin: 'Tools Endpoint - Fetch user tools',
						priority: 9
					})
				} else {
					return res.json({
						code: 'S0002',
						status: 'success',
						message: 'Records retrieved successfully.',
						data: data,
						origin: 'Tools Endpoint - Fetch user tools',
						priority: 10
					})
				}
			}). catch((error)=>{
			return res.json({
				code: 'S0003',
				status: 'failure',
				message: 'An error has occured.',
				data: error,
				origin: 'Tools Endpoint - Fetch user tools',
				priority: 8
			})
		})
	}
});


module.exports = router;