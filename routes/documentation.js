const express = require('express');
const router = express.Router();

router.get('/', (req, res, next) => {
    "use strict";
    res.redirect('/documentation/v' + process.env.CURRENT_DOCUMENTATION_VERSION + '/')
});

router.get('/v1', (req, res, next) => {
    "use strict";
    res.render('documentation/v1/index', { title: 'WTC_ Platform Documentation | V1' });
});

router.get('/v1/overview', (req, res, next) => {
    "use strict";
    res.render('documentation/v1/overview', { title: 'WTC_ Platform Documentation | Overview' });
});

router.get('/v1/users', (req, res, next) => {
    "use strict";
    res.render('documentation/v1/users', { title: 'WTC_ Platform Documentation | Users' });
});

router.get('/v1/users/create', (req, res, next) => {
	"use strict";
	res.render('documentation/v1/users/create', { title: 'WTC_ Platform Documentation | Create User' });
});

router.get('/v1/users/password-reset-init', (req, res, next) => {
	"use strict";
	res.render('documentation/v1/users/password-reset-init', { title: 'WTC_ Platform Documentation | Password Reset Initialization' });
});

router.get('/v1/users/password-reset', (req, res, next) => {
	"use strict";
	res.render('documentation/v1/users/password-reset', { title: 'WTC_ Platform Documentation | Password Reset' });
});

router.get('/v1/users/password-reset-token-verification', (req, res, next) => {
	"use strict";
	res.render('documentation/v1/users/password-reset-token-verification', { title: 'WTC_ Platform Documentation | Password Reset Token Verification' });
});

router.get('/v1/users/add-user-quicklink', (req, res, next) => {
	"use strict";
	res.render('documentation/v1/users/add-user-quicklink', { title: 'WTC_ Platform Documentation | Add user quicklink' });
});

router.get('/v1/users/authentication-verification', (req, res, next) => {
    "use strict";
    res.render('documentation/v1/users/authentication-verification', { title: 'WeThinkCode_ Documentation | User authentication verification' });
});

router.get('/v1/users/get-users', (req, res, next) => {
    "use strict";
    res.render('documentation/v1/users/get-users', { title: 'WeThinkCode_ Documentation | Retrieve all users' });
});

router.get('/v1/users/get-user', (req, res, next) => {
    "use strict";
    res.render('documentation/v1/users/get-user', { title: 'WeThinkCode_ Documentation | Retrieve a single user' });
});

router.get('/v1/users/password-reset-token-verification', (req, res, next) => {
    "use strict";
    res.render('documentation/v1/users/password-reset-token-verification', { title: 'WeThinkCode_ Documentation | User password reset token verification' });
});

router.get('/v1/users/get-user-quicklinks', (req, res, next) => {
    "use strict";
    res.render('documentation/v1/users/get-user-quicklinks', { title: 'WeThinkCode_ Documentation | Retrieve user quicklinks' });
});

router.get('/v1/users/activation', (req, res, next) => {
    "use strict";
    res.render('documentation/v1/users/activation', { title: 'WeThinkCode_ Documentation | User activation' });
});

router.get('/v1/users/password-reset', (req, res, next) => {
    "use strict";
    res.render('documentation/v1/users/password-reset', { title: 'WeThinkCode_ Documentation | Submitting new user password' });
});

router.get('/v1/users/remove-user-quicklink', (req, res, next) => {
    "use strict";
    res.render('documentation/v1/users/remove-user-quicklink', { title: 'WeThinkCode_ Documentation | Removing a user quicklink' });
});

router.get('/v1/users/authentication', (req, res, next) => {
    "use strict";
    res.render('documentation/v1/users/authentication', { title: 'WeThinkCode_ Documentation | User authentication' });
});



module.exports = router;
